# Film Module

The film contains the final image buffers at output resolution after render region cropping.
It stores all render passes results and the per pixel weight.

Here is a schematic of film extents and offsets looks like.

![Film Conventions](../img/film_naming_convention.svg)

In the code, we have:
* `display_extent` refering to either the render result extent.
* `film_extent` refering to either the render region extent.
* `render_extent` refering to either the render target extent.

## Jitter

For each render sample, we choose a random position inside the **render pixel area**. This random offset is applied to the projection matrix (`winmat` in the code) and render pixels colors are computed.

## Resampling

After rendering, the film samples the neighboring rendered pixels and weight them according to the pixel filter. The temporal re-projection and firefly filter is optionally done at this stage.
The set of neighbor pixels to read and their weights are precomputed on CPU as they are all the same for all final pixels.

![Final Pixels Resampling](../img/film_resampling.svg)

For data passes only the closest sample is kept and no weighted average is performed, avoiding value changes.

## Up-scaling

When using up-scaling (`Render Settings > Performance > Viewport > Pixel Size`), the render target is not at native resolution. The weights cannot be pre-computed and are recomputed per final pixel on the GPU.

![2x Up-sampling of the render target](../img/film_upsampling.svg)
