# Pipelines

A pipeline is a combination of one or multiple `draw::Pass` along with some internal logic for rendering. The pass types an object can subscribe to is listed by `eMaterialPipeline`. The objects subscribe to the `draw::Pass` with its associated shaders. 

A single object can subscribe to multiple pipelines. A renderable object type is called a geometry type and listed in `eMaterialGeometry`.

Pipelines are filled with scene data and are view agnostic. Multiple views can call the same pipeline (e.g. the 6 views of a cubemap). View dependant resources should be stored inside [views](../view.md) and passed to the `render()` method of the pipelines (e.g. `ShadingView::rt_buffer_opaque_`).

## Material Pipeline

A core design principle of EEVEE is that all pipelines should be compatible with all geometry type (with a few exceptions).

```mermaid
flowchart TD
    A[Mesh]
	B[Curves]
	D[Point Cloud]
	C[Volume]
	E[World]
	
    classDef geometry stroke:#f66
    class A,B,C,D,E geometry;
	
	I[Deferred]
	J[Forward]
	K[Shadow]
	L[Volume]
	M[Background]
	N[World]
	
    classDef pipeline stroke:#66f
    class I,J,K,L,M,N pipeline;
	
	E --> L & M & N
	C --> L
	A & B & D --> Reroute{Solid} --> I & J & K & L
	
	S[Prepass]
	T[Gbuffer]
	U[Prepass]
	V[Shading]
	W[Occupancy]
	X[Material]
	Y[Shadow]
	Z[World]
	
    classDef shader stroke:#6f6
    class S,T,U,V,W,X,Y,Z shader;
	
	I --> S & T
	J --> U & V
	K --> Y
	L --> W & X
	M & N --> Z
```
Red is geometry type, blue is pipeline type, green is shader pass type.

Internally, the pass type maps to a fragment shader and the geometry type to a vertex shader. Some geometry type still need some special code in the fragment stage, and vice-versa, some pipelines need adjustments of the vertex shader. For this reasons, the `MAT_GEOM_*` (for geometry types) and `MAT_*` (for pipeline types) exists.

