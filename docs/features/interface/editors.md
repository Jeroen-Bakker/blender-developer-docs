# Editors

What we normally call "editors" are a **ScrArea** struct containing the
data needed for an editor including type, data, position, size,
callbacks, and more. Each editor is in a list of them in a
[Screen](screen.md) (areabase), which is in a
[Window](window.md). It is the data, regions,
and callbacks that make each editor unique.

## Editor Unique Data

Since each editor is different, they all store data in a struct that
derives (implicitly) from `SpaceLink`. ScrArea has a *list* of these
called `spacedata` and it is the *first* of these that is used by the
active editor. It is a list so that it can contain the data of every
editor that has occupied the same space, allowing quick switching
between them.

## Editor Callbacks

The ScrArea struct contains a `type` member that is a pointer to a
`SpaceType`. This mostly contains callbacks for when the editor is
created, freed, activated, refreshed, saved, etc. It also contains the
area's name, icons, keymaps, dropboxes, and much more.

## Other ScrArea Contents

Some other parts of the ScrArea are the ScrVerts, which define each
outer corner and can be shared with immediate neighbors. `totrct`
defines the interior visible part of the editor in window space. There
is also a `winx`, `winy` containing the width and height
respectively.

It contains both a `butspacetype` and `spacetype` members that
contains an indicator of the type of editor, from `eSpace_Type`. These
two are used in slightly different ways that are not entirely apparent
to this writer (Harley). `butspacetype_subtype` contains the "subtype"
for editors that have alternate views.

## Editor Source Files

The source files for each editor are kept together in a folder with a
name like `source/blender/editors/space_xxx` where "xxx" is the name
of the editor. The main file is one named like "space_xxx.cc".
`ED_spacetypes_init` calls a function like "ED_spacetype_xxx" that
defines all the editors regions, sizes, handlers, and callbacks, etc.

## API

For more specific information, look at these files:

``` cpp
source/blender/makesdna/DNA_screen_types.h
source/blender/editors/include/ED_space_api.hh
source/blender/editors/include/ED_screen.hh
source/blender/editors/screen/screen_intern.h
source/blender/editors/space_api/spacetypes.cc
source/blender/editors/screen\area.cc
source/blender/editors/screen/screen_ops.cc
```
