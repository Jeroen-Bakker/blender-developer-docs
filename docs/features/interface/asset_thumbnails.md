# Asset Thumbnails

This page is about the thumbnails of the 'Essentials' assets that come built-in with Blender.

## Brush Thumbnail Style Guide

The thumbnails of brushes were redesigned for the Blender 4.3 release. This section explains the important aspects of the new visual style and how to best recreate them.

The files used for creating them [can be found in the git repo](https://projects.blender.org/blender/blender-assets/src/branch/main/working/brushes/thumbnails). This includes:

- The .blend file to recreate existing thumbnails
- The `matcap_warm_clay_bright.exr` used for viewport shading
- A reference weight paint gradient
- All existing high resolution thumbnails

### Iconic and Detailed

The thumbnails have to work on a wide variety of scales.   
Dozens of brushes have to fit into the asset shelf while being recognizable at a glance. Variation in shape, detail, flow and color are the key to making brushes look distinct.

![Various brushes should in the Asset Shelf](../../images/thumbnail_asset_shelf_small.png)

Meanwhile, detailed brushes for use with textures must show their effects very well on a large scale.

![An example of an anchored brush with a detailed skin texture](../../images/thumbnail_large.png)

The general functionality of the brush should work on an icon scale (similar to the toolbar), while specific effects can be shown in the details. If some aspects of the brush behavior cannot be communicated purely by the geometry, then additional graphical arrows and lines are added to communicate meaning.

### Ease of Creation

The thumbnails were designed to be easily created in the viewport at any time. All brush thumbnails were rendered directly out of the viewport.

Matcaps (for meshes & curves) and flat shading (for Grease Pencil) are used for an instant lighting setup. Vertex colors and Masks are created to create contrast and accent colors.

Any colored graphical elements are created with Grease Pencil or even the Annotate tool.

This way anyone can create their own thumbnails quickly and still make them immediately fit in.

### Selective Contrast

Geometry that is affected by brushes must be brighter than unaffected areas. This way the effect the brush has is visible at a glance.
This is usually done via sculpt masks or vertex painting.

![](../../images/thumbnail_dos_donts_images_contrast.png)

In the case that the brush is also painting with color, the affected geometry should have a blue accent. Other colors should only be reserved for brushes that paint with a particular color texture. And in that case, unaffected areas should still be dark to keep the same contrast.

![](../../images/thumbnail_dos_donts_images_paint.png)

Ideally don't angle bright areas too much. The Matcap shading would result in darker surfaces as a result.
Make the affected geometry face towards the camera or slightly upwards.

There are exceptions to the use of dark areas, like the 'Elastic' sculpting brushes and certain curve sculpting brushes like 'Comb'. This is meant to show that they have a far reaching effect on geometry outside of their immediate brush radius.

![](../../images/thumbnail_dos_donts_images_contrast_no_mask.png)

### Transparent Background

The thumbnail images are saved as .png files with a transparent background. This way the thumbnails blend well into various themes and are generally showing the blue selection highlight more clearly.

![](../../images/thumbnail_dos_donts_images_transparency.png)

### Grease Pencil Style

Grease Pencil thumbnails on the other hand use a flipped contrast. The strokes are generally black, just like they would appear when drawing them.

![](../../images/thumbnail_dos_donts_images_gp_contrast.png)

Since it's common to use draw brushes with custom materials and textures, the background is a fully opaque light grey. This ensures that most material colors can be displayed. The background value can be  varied between brushes to accommodate for hard to see colors, like light colors.

### Focused Framing

The thumbnails are always focused on the visible effect of the brush stroke. This means that there is no consistent sphere shape or camera view between thumbnails. The depiction is much more free form and zoomed in.

![](../../images/thumbnail_dos_donts_images_zoom.png)

Ideally the bright areas should be contained within a screen margin of 0.16 of the width/height on all sides. This gives a minimal amount of consistency and makes the thumbnails not overly zoomed in.

### Ascending Direction

Another consistent element between thumbnails is the left-to-right and bottom-to-top direction of the depicted geometry.

![](../../images/thumbnail_dos_donts_images_angle.png)

This isn't completely strict. The brush stroke and more complex geometry can go against it to create more variation. Some Grease Pencil thumbnails also go against it for this reason. But the generic flow of the thumbnails should keep this alignment to some extend.

### Intentional Repetition

Avoid repeating the same shape language with strokes and geometry!

The shapes of the bright areas need to be distinct and recognizable. The usual argument against this is that consistent stroke can make the style and differences between brushes clearer. But this is only true in the details. Overall this makes the brushes look the same and hard to identify at a glance.

![](../../images/thumbnail_dos_donts_images_wide_stroke_variation.png)

The same goes for the geometry that the brush is used on. Not all meshes need to be a sphere. Use whatever primitive shape is most suitable to understand the effect of the brush stroke and its uses. This also further helps to make each thumbnail distinct.

![](../../images/thumbnail_dos_donts_images_wide_shape_variation.png)

That doesn't mean that every brush needs to look unique. Some brushes can be seen as siblings because they share the same use or functionality. In that case it's great to make them look similar.
This is especially useful between different modes and object types, since this makes the brushes instantly familiar.

![](../../images/thumbnail_dos_donts_images_repetition.png)

### Colored Accents

Color on additional graphical elements is used to communicate otherwise hidden meaning and group similar types of brushes.

![An overview of all colors and their meaning. The meaning can vary a bit between modes.](../../images/thumbnail_dos_donts_images_color.png)

Most brushes will not have a color accent. In the case of sculpting, these brushes are regular drawing brushes that add or subtract to a surface. For Grease Pencil these are drawing brushes to create new strokes or sculpting brushes with a very straight forward effects.

Painting brushes are an exception where the accent color is in the affected geometry itself instead of extra elements.

The HEX colors for each accent are as follows:

- **Blue** =
  - **3D** = `#d5e8ff`
  - **GP** = `#abd1ff`
- **Yellow** = `#ffed9f`
- **Green** =
  - **3D** = `#71ff98`
  - **GP** = `#75e093`
- **Red** = `#ff7d61`
- **Purple** = `#e1a2ff`
- **Orange** = `#ffc46c`

Any graphical elements are best kept to the transparent background or darker areas of the thumbnail for visibility.

![](../../images/thumbnail_dos_donts_images_annotations_contrast.png)

### Weight Gradient

For weight painting brushes the heatmap gradient is used. For meshes a custom gradient is defined in the preferences to make the colors more pleasing and fit into the default themes.

For Grease Pencil weight painting, the heatmap is shown via points within the stroke.

![Weight paint gradients on meshes and Grease Pencil.](../../images/thumbnail_weight_gradients.png)
