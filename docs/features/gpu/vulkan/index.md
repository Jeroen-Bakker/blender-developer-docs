# Vulkan backend

Vulkan backend is part of the `gpu` module. It enables drawing Blenders
UI using Vulkan. It doesn't (and isn't planned or viable) to use Vulkan
for running Cycles. 

## Command line parameters

`--debug-gpu-vulkan-local-read`:
    Force enabling `VK_EXT_dynamic_rendering_local_read` extension even
    if the platform is listed to not use it. This allows checking changes
    done to the implementation on devices that normally doesn't use the
    extension due to performance regressions.


## Compile directives

In CMAKE there are several compilation directives specific for the
Vulkan backend.

`WITH_VULKAN_BACKEND` will enable/disable compiling Blender with Vulkan
support. Mainly for Apple this is turned off.

`WITH_VULKAN_MOLTEN_VK` is still very experimental. Current state is
that it can draw a UI, but is not usable at all as HiDPI isn't supported
and due to many limitations in MoltenVK the 3D viewport is only able to
draw wireframes and outlines.

`WITH_RENDERDOC` is shared with OpenGL, but specific for Vulkan it adds
more features like vertex/fragment/compute step by step debugging and
colored debug groups.

`WITH_GPU_RENDER_TESTS_VULKAN` also include vulkan tests when performing
the GPU render tests. This is turned off by default as the tests currently
can hang and fail.

## Components

Vulkan backend consists out of several components that work together.

- Memory manager
- Data management, data conversion, lifetime management
- Shader compilation
- Shader resources/binding
- [Render graph](render_graph.md)
- Rendering
- Threading
- [Device management](device_management.md)


## Extensions

Vulkan works with extensions. Extensions are delivered by the GPU driver.
Blender requires some extensions to be available otherwise Blender cannot
start. Other extensions are optional.
