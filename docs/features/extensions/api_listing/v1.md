# v1

## JSON

```json
{
    "version": "v1",
    "blocklist": [],
    "data": [
    {
       "id": "blender_kitsu",
       "name": "Blender Kitsu",
       "tagline": "Pipeline management for projects collaboration",
       "version": "0.1.5-alpha+f52258de",
       "type": "add-on",
       "archive_size": 856650,
       "archive_hash": "sha256:3d2972a6f6482e3c502273434ca53eec0c5ab3dae628b55c101c95a4bc4e15b2",
       "archive_url": "https://extensions.blender.org/add-ons/blender-kitsu/0.1.5-alpha+f52258de/download/",
       "blender_version_min": "4.2.0",
       "maintainer": "Blender Studio",
       "tags": ["Pipeline"],
       "license": ["SPDX:GPL-3.0-or-later"],
       "website": "https://extensions.blender.org/add-ons/blender-kitsu/",
       "schema_version": "1.0.0"
    }
    ]
}
```

### Required values

| Field | Description | Example |
| ----- | ----------- | ------- |
| <b>version</b> | API version of the file. | `"v1"` |
| <b>blocklist</b> | List of extensions that should be disabled by Blender. | `["bad_extension", "another_bad_extension"]` |
| <b>data</b> | List of dictionaries with the extensions information. | `{"id"="example_extension", ...}` |

NOTE:
The content of data depends on the [schema](../schema/index.md) for the respective extension. And similar to it,
the optional fields (e.g., ``blender_version_max``) are either to have a value or should be omitted from the entries.

For the official Extensions Platform, the ``website`` value is the page of the extension in the online platform.
Even if the manifest points to the project specific website.

---

Generate this JSON file by using the
[command-line](https://docs.blender.org/manual/en/dev/advanced/extensions/command_line_arguments.html)
tools.

## Changelog
* Initial version.
