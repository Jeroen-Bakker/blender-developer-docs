# Cycles Source Layout

## Modules

![High level overview of Cycles modules](../../images/Cycles-modules-architecture.png){style="width:600px;"}

## Folders

The source code is organized into folders as follows:

| Folder              | Description                                                                   |
|---------------------|-------------------------------------------------------------------------------|
| app/                | Standalone session and network rendering server applications.                 |
| blender/            | Python module and C++ extension to integrate with Blender.                    |
| blender/addon       | Python scripts for the Blender addon.                                         |
| bvh/                | Bounding volume hierarchy constructions for raytracing.                       |
| cmake/              | Build and release files.                                                      |
| device/             | Device abstraction.                                                           |
| device/cpu/         | CPU device.                                                                   |
| device/cuda/        | CUDA device.                                                                  |
| device/dummy/       | Dummy device.                                                                 |
| device/hip/         | HIP device.                                                                   |
| device/hiprt/       | HIP RT device.                                                                |
| device/metal/       | Metal device.                                                                 |
| device/multi/       |                                                                               |
| device/oneapi/      | oneAPI device.                                                                |
| device/optix/       | Optix device, based on CUDA.                                                  |
| doc/license/        | Licenses.                                                                     |
| integrator/         | Host side code for path tracing integrator, invoking rendering kernels.       |
| graph/              | Generic scene node graph.                                                     |
| kernel/             | Rendering kernel, that can run on the CPU or GPU.                             |
| kernel/closures/    | BSDF closures used by svm and osl shader backend.                             |
| kernel/device/      | Kernel code specific to CPU and GPU devices.                                  |
| kernel/integrator/  | Path tracing integrator kernels.                                              |
| kernel/svm/         | Shader virtual machine to execute shader graphs on the CPU and GPU.           |
| kernel/osl/         | OSL render services and shading engine, only runs on the CPU.                 |
| kernel/osl/shaders/ | OSL shaders used in the shader graph.                                         |
| scene/              | Render scene and its nodes.                                                   |
| session/            | Render session and associated data structures like drivers and render buffers |
| subd/               | Tesselation and subdivision surfaces.                                         |
| test/               | Unit tests.                                                                   |
| util/               | Utility code.                                                                 |
