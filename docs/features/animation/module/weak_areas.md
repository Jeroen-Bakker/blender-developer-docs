# Animation & Rigging: Weak Areas

Here module members can keep track of weaker areas in the animation
system.

**NOTE:** This is not meant to be an exhaustive list of all known
issues. We have [the issue tracker][issues] for that.

[issues]: https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=268%2c296

## Animation Channel Filtering

- [#50061: Mask Opacity keyframes not visible
anywhere.](https://projects.blender.org/blender/blender/issues/50061)

## Auto-keying

The auto-keying system appears to have been designed for transforms
only, and as a result doesn't work well for other properties.

- Transform operations can cause keying of non-transform properties, and
  non-transform operations often don't even auto-key
  ([T88066](http://developer.blender.org/T88066): Auto Keying with "Only
  Active Keying Set" creates keyframes it shouldn't)
- Custom keying sets are always absolute (in terms of which properties
  of which objects are keyed), instead of the often-expected "the named
  property of the active/selected objects".
- [T91134](http://developer.blender.org/T91134): Change autokey
  preferences "Only insert available" to "Only insert keyframes on
  existing F-curves"
- [T91135](http://developer.blender.org/T91135): Change autokey
  preferences "Only insert needed" to "Only insert keyframes on
  properties that have changed."
- [T91941](http://developer.blender.org/T91941): "Whole Character"
  keying set not working properly with autokey enabled


## Animation Cycles

Animation Cycles are an integral part of an animators life. In Blender they are
technically supported, but suffer from poor visibility in terms of UX. In order
to set up a scene for cyclic animation you have to:

- Enable "Cycle Aware Keying".
- Have an action on your armature and enable "Manual Frame Range" and "Cyclic
Animation" on the Action.
- Give the action the desired range.

When all that is set, Blender will add the "Cycles" FCurve modifier whenever an
FCurve is created for the action. For this to work Blender has to ensure there
are at least 2 frames on an FCurve.

Issues (not all valid bugs, but definitely usage issues)

- blender/blender#111322 Insert keyframe wrong with cycle-aware keying after
deleting end frame

Design Tasks

- blender/blender#54724 Workflow improvements for creating Animation Loops

failed PRs

- blender/blender!116356 Fix: Cycle aware keying inserting first key

## Mirroring and Symmetry

- [T61386](http://developer.blender.org/T61386): Mirror Paste for pose
  bones also copies custom properties
- [T65671](http://developer.blender.org/T65671): Armature X-Mirror
  inconsistencies
- [T74032](http://developer.blender.org/T74032): Pose Mode X-Axis Mirror
  is not working properly with IK when skeleton is moved
- [T76915](http://developer.blender.org/T76915): Pose bone: clearing
  transformations in pose mode does not affect the mirrored bones even
  when the X-axis mirror is activated.
- [T83657](http://developer.blender.org/T83657): In pose mode: Mirror X
  doesn't work with certain operations


## Selection synchronization between pose bones and animation channels

- [T48145: All bone drivers selected when reselecting bone](https://developer.blender.org/T48145)
- [T58718: Clicking on Dope Sheet deselects all bones](https://developer.blender.org/T58718)
- [T62463: Skeleton rig with keyframes prevents selection of Shader Nodetree channels in Dope Sheet and Graph Editor](https://developer.blender.org/T62463)
- [T71615: Select key in dopesheet deselect bone in the viewport](https://developer.blender.org/T71615)
- [T73215: Blender autokeying deselects objects channels but not Armatures](https://developer.blender.org/T73215)

In short, there is space for improvement regarding the synchronization
between the selection state of bones (in the viewport) and channel rows
(in the dope sheet/action editor/etc). Hjalti mentioned that it's
probably better to synchronize less, that is, have the selections more
separate. This new behavior would need to be properly designed &
discussed, though.

## Drivers and Matrix decomposition
Because drivers (and constraints) use matrix decomposition to derive values (e.g. scale X) some driver configurations do not work as expected. For example negative scales might get lost.
- [#124893: DriverVars reading local Y scale are never negative](https://projects.blender.org/blender/blender/issues/124893)
- [#124896: Limit Scale constraint minimum doesn't prevent negative scaling](https://projects.blender.org/blender/blender/issues/124896)
