# PBVH

Sculpt mode uses a coarse BVH tree, `PBVH`. The leaves are fairly
large compared to most BVHs; this is because the leaves are used to
split the GPU mesh.

## Data modes

PBVH has three different modes, each of which has a different data
backing:

- `bke::pbvh::Type::Mesh`: [Mesh](../objects/mesh/mesh.md) data.
- `bke::pbvh::Type::BMesh`: Dynamic topology, uses [BMesh](../objects/mesh/bmesh.md).
- `bke::pbvh::Type::Grids`: Multiresolution data.

You get can the mode with `bke::object::pbvh_get`.