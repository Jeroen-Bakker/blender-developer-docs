# macOS Libraries Maintenance

## ARM

The basic thing to build libraries is to run `make deps`, which will
download and compile all libraries in `../build_darwin/deps_arm64`.

Most likely it will tell you that you are missing some tools, and give
you homebrew commands to install them.

After this is done it will automatically install the updated libraries
to `lib/macos_arm64` from where they can be committed.

## x64 Cross Compilation

x64 libraries are currently cross compiled on ARM, which is
convenient to have on a single machine.

### Homebrew

This requires two homebrew installs, one for arm and one for x86\_64.
They will be installed in different folders automatically.

Install Rosetta if you haven't already:
``` bash
/usr/sbin/softwareupdate --install-rosetta --agree-to-license
```

macOS then supports dropping into a x86\_64 terminal like this.

``` bash
arch -x86_64 zsh
```

From here you can install homebrew a second time.

Then add the following in `~/.zshrc`, so it automatically picks
up the right homebrew install depending on the arch.

``` bash
ARCH=$(arch)
if [[ $ARCH == "i386" ]]; then
  eval "$(/usr/local/bin/brew shellenv)"
else
  eval "$(/opt/homebrew/bin/brew shellenv)"
fi
```

This probably replaces some existing code put there by Homebrew.

### Building

Always drop into the x86\_64 terminal first.

``` bash
arch -x86_64 zsh
```

With this, `make deps` will generate a build in `build_darwin/deps_x64`.

## Validation

The libraries should be self contained, and only linking to macOS system libraries.
And for example not anything in `/opt/homebrew`.

This can be checked as follows:
```
otool -L lib/macos*/**/*.dylib
otool -L ../build_darwin/bin/Blender.app/Contents/MacOS/Blender
```

## LTS and CVEs

We committed to updating libraries when security issues for them are
found, both for main and LTS releases.

Checking for such issues is automated by `make cve_check` in the
libraries build folder.

If you have enough disk space you may consider setting up an entirely
separate build and lib folder for LTS since switching with main is
painful, as you will need to clear the entire build folder for it to
rebuild correctly.

## Other Notes

* Consider installing Xcode by manually downloading it from the Apple developer site
  instead of the app store, to avoid unexpected automatic updates when the time is not convenient.
* Command Line Tools currently don't work as OpenImageDenoise requires the
  Metal compiler tools that are only included with Xcode.
* On macOS we do not use versioned library symlinks because Apple notarization
  rejects them for unknown reasons. The builds scripts automatically strip these and
  patch the dylibs to remove them.
* Before committing big changes, push the libraries to a branch and make a PR
  with the corresponding hash. This way the libraries can be tested to work on the buildbot.
  For testing locally, use `make developer` and ensure test files are available,
  to be as close as possible to the buildbot.
* There is some automatic rebuilding when dependencies of libraries change,
  but it does not account for everything. Often you may need to remove the `build` and `Release`
  folder to ensure things really rebuild, or particular folders contained in them
  if you know which specific ones need a rebuild.
