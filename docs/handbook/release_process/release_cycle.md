---
hide:
  - toc
---

# Release Cycle

## Release Dates and Schedule

For information on the release dates of specific Blender versions, see the
[milestones on
project.blender.org](https://projects.blender.org/blender/blender/milestones).

## Release Cycle

A new Blender version is targeted to be released every 4 months. The
actual release cycle for a specific release is longer, and overlaps the
previous and next release cycle.

![](../../images/Blender_release_cycle_01.png)

### Branches

Work is done in two branches:

- `blender-v{VERSION}-release` branch: bug fixing only for the
  upcoming or LTS release
- `main` branch: new features and improvements for the release after
  that

The `blender-v{VERSION}-release` branch will be available for 6 weeks
prior to the release date. At the same time `main` will be open for
the next release, giving 17 weeks to add new features and improve them for the next
release.

## Release Phases

Each Blender version has its own release phase, indicating which types of
changes are allowed to be committed and what developers are focusing on.

That means for example that Blender 4.0 can be in Beta (bug fixing
only) while Blender 4.1 is in Alpha (open for new features).


<table markdown>
<tbody markdown>
<tr class="odd" markdown>
  <td markdown>**Phase**</td>
  <td markdown>**Description**</td>
  <td markdown>**Duration**</td>
  <td markdown>**Details**</td>
  <td markdown>**Branch**</td>
  <td markdown>**Splash Shows**</td>
</tr>
<tr class="even" markdown>
  <td rowspan="2">Alpha</td>
  <td rowspan="2">New features and changes</td>
  <td>6 weeks</td>
  <td rowspan="2">Add bigger features, update libraries and merge branches.
  <br><br>
  Risky changes that are likely to cause bugs or additional work
  must be done within the first few weeks of this phase.
  Afterwards only smaller and less risky changes, including small features, should be made.
  <br><br>
  The first 6 weeks overlap with the Beta &amp; Release Candidate phases of the previous
  release, and many developers will focus on bug fixing.
  <br><br>
  If a new feature is too unstable or incomplete, it will be reverted before the
  end of this phase.
  <br/><br/>
  Developers dedicate a steady 1-3 days/week to the bug tracker, focusing on
  triaging and newly introduced bugs.
  </td>
  <td rowspan="2" markdown>`main`</td>
  <td rowspan="2">Alpha</td>
</tr>
<tr class="odd">
  <td>11 weeks</td>
</tr>
<tr class="odd" markdown>
  <td>Beta</td>
  <td>Bug fixing only</td>
  <td>5 weeks</td>
  <td markdown>Focus on bug fixing and getting the release ready.
  <br/><br/>
  Development moves to the `blender-v{VERSION}` [release branch](release_branch.md) and the splash screen gets updated. In `main` Alpha for the next release starts. `blender-v{VERSION}-release` is regularly merged into `main`.
  <br/><br/>
  The Python API remains stable during this phase, so add-ons have time to
  update before the release.
  <br/><br/>
  High severity bugs dictate how much time developers will spend in the tracker
  as opposed to new features for the next release.
  </td>
  <td markdown>`blender-v{VERSION}-release`</td>
  <td>Beta</td>
</tr>
<tr class="even" markdown>
  <td>Release Candidate</td>
  <td>Prepare release</td>
  <td>1 week</td>
  <td>Stable branch is frozen to prepare for the release. Only critical and
  carefully reviewed bug fixes allowed.
  <br/><br/>
  Release candidate and release builds are made.
  <br/><br/>
  Developers spend a short time 5 days/week with an eye in the tracker for any
  unexpected high severity regression.
  </td>
  <td markdown>`blender-v{VERSION}-release`</td>
  <td>Release Candidate</td>
</tr>
<tr class="odd">
  <td>Release</td>
  <td>Release</td>
  <td>1-2 days</td>
  <td>Stage where the final builds are packaged for all platforms, last tweaks
  to the logs and promotional images, social media, video announcements.
  <br/><br/>
  The final switch is flicked on <a href="https://www.blender.org">blender.org</a> for the new release to show up
  in the <a href="https://www.blender.org/download">Download page</a>.
  </td>
  <td>-</td>
  <td>-</td>
</tr>
<tr class="even" markdown>
  <td>LTS</td>
  <td>Long-term release</td>
  <td>2 years</td>
  <td markdown>Critical fixes from `main` are ported over after passing quality assurance tests.
  For actual [LTS releases](lts.md) only.
  </td>
  <td markdown>`blender-v{VERSION}-release`</td>
  <td></td>
</tr>
</tbody>
</table>
