# Extension Hosting on blender.org

The general recommendation is for
[Extensions](https://docs.blender.org/manual/en/dev/extensions/index.html)
to be hosted on personal repositories on
[projects.blender.org](https://projects.blender.org) or external host services.

In some cases, however, these extensions can be hosted on official `blender.org` repositories:

* Extensions which are mostly maintained by Blender developers.
* Extensions which are popular but have no active maintainer at the moment
  (e.g., some of the add-ons bundled with Blender until the version 4.1).

All of those extensions will be hosted on
[projects.blender.org/extensions](https://projects.blender.org/extensions/).
Each extension should have its own repository and issue tracker.

## Create a new repository

* Create a new repository on
  [projects.blender.org/extensions](https://projects.blender.org/extensions/).
* It is recommended for the repository to have the `main` branch protected, with "Enable Push".
* The add-on maintainers need to be added as Collaborators for the repository.


## Migrate from the add-ons repository

In some cases add-ons need to be extracted from a repository
where other add-ons were also developed.
The following steps show how to do so while preserving the git log history.

### Get a fresh copy of the add-ons

```sh
git clone https://projects.blender.org/blender/blender-addons.git
cd blender-addons
```

!!! Warning
    Some of the commands here may ruin your existing repository.
    Work with a new copy of the repository to avoid any troubles.

### Create a new branch only with the folder you want

The command is:
```sh
git subtree split -P <source_folder> -b <target_branch>
```

Let's for example create a new repository for the `io_shape_mdd` add-on.

```sh
git subtree split -P io_shape_mdd -b io_shape_mdd_new
```

In this example it will copy:
```
   ./io_shape_md/
   ├─ __init__.py
   ├─ export_mdd.py
   └─ import_mdd.py
```

Into:
```
   ./__init__.py
   ./export_mdd.py
   ./import_mdd.py
```

The new branch (io_shape_mdd_new) will have its original git history preserved.


!!! Extra
    If you want to keep the files under the same subfolder,
    use [git filter-repo](https://github.com/newren/git-filter-repo) instead:
    ```
    git checkout -b io_shape_mdd_new
    git filter-repo --path io_shape_mdd
    ```

### Single file add-ons

For add-ons that only contain a single file you need to use
[git filter-repo](https://github.com/newren/git-filter-repo).

```
git checkout -b depsgraph
git filter-repo --path-rename depsgraph_debug.py:__init__.py
```

This will create a new branch (depsgraph) with a single `__init__.py` file,
containing all the git history from the original file (`depsgraph_debug.py`).
Extension add-ons always require a `__init__.py`.

Alternatively you can just get the file:

```
git checkout -b depsgraph
git filter-repo --path depsgraph_debug.py
```

And create a `__init__.py` file later to load your extension.
That's beyond the scope of this guide though.

### Push the repository online:

First you need to make sure you have access to the repository.

```
git remote add io_shape_mdd.git git@projects.blender.org:extensions/io_shape_mdd.git
git push -u io_shape_mdd.git io_shape_mdd_new:main
```

## Publishing extensions

Now that the `.git` history is clear and the extension can be developed further,
you can start making an extension.
To convert an add-on into an extension follow
[those steps](https://docs.blender.org/manual/en/dev/extensions/addons.html#converting-a-legacy-add-on-into-an-extension).

### Upload the extension

Upload the extension to [extensions.blender.org](https://extensions.blender.org/).
If you need multiple contributors
to co-maintain the extension on the platform please contact a moderator on
[#extensions-moderators](https://chat.blender.org/#/room/#extensions-moderators:blender.org).

After you upload the extension, remember to use the repository own issue tracker as the support
URL on the [Extensions Platform website](https://extensions.blender.org/).
