# Chat

Many Blender developers and users hang out on [chat.blender.org](https://chat.blender.org).
Take a look at the [directory](https://chat.blender.org/#/directory) for an overview of available channels. The most popular ones
are listed below.

>? INFO: **Issues with chat.blender.org?**
>
> If you experience issues on chat.blender.org itself go to the
> [\#blender-chat-meta](https://chat.blender.org/#/room/#blender-chat-meta:blender.org) room for help.

## Developer Channels
Developer channels are where developers and other contributors discuss ongoing
development. Please stay on topic in these channels.

### General Development

**[\#blender-coders](https://chat.blender.org/#/room/#blender-coders:blender.org)**:
Official Blender development chat room for discussions about Blender
core development, where most Blender developers hang out.

**[\#docs](https://chat.blender.org/#/room/#docs:blender.org)**: Official Blender
documentation chat room where discussions about the Blender manual.

**[\#python](https://chat.blender.org/#/room/#python:blender.org)**: If you need help
regarding Blender's Python functionality, this is the place to visit.

**[\#translations](https://chat.blender.org/#/room/#translations:blender.org)**:
Official Blender channel for discussing the translation of the Blender
user interface and manual.

**[\#blender-builds](https://chat.blender.org/#/room/#blender-builds:blender.org)**:
Need help compiling the Blender source code? Although it's quick and
easy for some people, not everyone is as lucky getting Blender to build
on their platform. Drop by if you need assistance.

### Modules

Module channels are where developers and other contribute discuss ongoing
development.

**[\#module-animation](https://chat.blender.org/#/room/#module-animation:blender.org)**:
Animation development

**[\#module-asset-system](https://chat.blender.org/#/room/#module-asset-system:blender.org)**:
Asset system development

**[\#module-compositor](https://chat.blender.org/#/room/#module-compositor:blender.org)**: Compositor development, as part of the VFX & Video module

**[\#module-core](https://chat.blender.org/#/room/#module-core:blender.org)**: Core
module development

**[\#module-eevee-viewport](https://chat.blender.org/#/room/#module-eevee-viewport:blender.org)**:
EEVEE & Viewport development

**[\#module-grease-pencil](https://chat.blender.org/#/room/#module-grease-pencil:blender.org)**:
Grease Pencil development

**[\#module-nodes-physics](https://chat.blender.org/#/room/#module-nodes-physics:blender.org)**:
Nodes & Physics development

**[\#module-pipeline-io](https://chat.blender.org/#/room/#module-pipeline-io:blender.org)**:
Pipeline & IO development

**[\#module-render-cycles](https://chat.blender.org/#/room/#module-render-cycles:blender.org)**:
Render & Cycles development

**[\#module-sequencer](https://chat.blender.org/#/room/#module-sequencer:blender.org)**:
Video Sequence Editor development, as part of the VFX & Video module

**[\#module-sculpt-paint-texture](https://chat.blender.org/#/room/#module-sculpt-paint-texture:blender.org)**:
Sculpt, Paint and Texture development

**[\#module-triaging](https://chat.blender.org/#/room/#module-triaging:blender.org)**:
Triaging team coordination (bugs and pull requests)

**[\#module-user-interface](https://chat.blender.org/#/room/#module-user-interface:blender.org)**:
User Interface development

## Rules
Please follow the [code of conduct](code_of_conduct.md).

* Be patient if you don't get a reply immediately, people are not at their computer all day.
* Do not cross post the same message into multiple channels. We're a pretty small community, the same people will see your question regardless of where you post.
* Stay on topic. There are dedicated rooms for developers and for artists.






