# Code of Conduct

These guidelines apply to all official communication channels
(blender.org, chat.blender.org, mailing lists) and activities that take
place on blender.org.

Reports of violations can be sent to <moderators@blender.org> and will
get handled on a case by case basis. The moderators team is made up of
[Blender Admins and Development Coordinators](../organization/index.md).
If you wish to dispute moderator's decisions you can ask for a final
appeal at the Blender Foundation board, by contacting
[foundation@blender.org](mailto:foundation@blender.org).

We recognize that misunderstandings and mistakes can happen. We will
only take actions after repeated intentional violations.

## Community Guidelines

**Be respectful and considerate:**
: Disagreement is no excuse for poor behavior or personal attacks.

  Remember that a community where people feel uncomfortable is not a
productive one.

**Be patient and generous:**
: If someone asks for help it is because they need it.

  Do politely suggest specific documentation or more appropriate venues
where appropriate, but avoid aggressive or vague responses.

**Assume people mean well:**
: Remember that decisions are often a difficult choice between competing
priorities.

  If you disagree, please do so politely.

  If something seems outrageous, check that you did not misinterpret it.

  Ask for clarification, but do not assume the worst.

**Try to be concise:**
: Avoid repeating what has been said already.

  Making a conversation larger makes it difficult to follow, and people
often feel personally attacked if they receive multiple messages telling
them the same thing.
