# Commit Rights

If you have commit rights, it means you can yourself push changes to the
Blender repository (or one of the other repositories).

For those interested in commit rights - there is a group of [project
administrators](../organization/index.md#admins) that have to approve
all new committers, together with the module owner of the module that
the new developer will work on.

The basic method to get commit rights is to submit many and useful
quality patches that show that you have a good understanding of the
relevant code, even better is being someone who has a demonstrated
willingness to fix bugs. Also it is good to show that you can work with
the team some ways of showing this are:

- Responding to criticism and suggestions regarding your code in a
  professional and well reasoned manner
- Making reasonable changes that are requested or explain why the change
  isn't reasonable/feasible
- Ensuring that your patches don't add warnings (and preferably remove
  warnings that already existed in that code area) and that any warnings
  found after you commit are cleaned up quickly.

See git configuration for write access on
[Tools/Git#Commit_Access](using_git.md#commit-access)
