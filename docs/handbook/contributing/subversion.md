# Subversion

> INFO: For Blender releases under development, all repositories have moved to
  Git LFS. This is only relevant for building older releases.

For older releases, there are Subversion repositories with libraries and
test data. These repositories also contain some old projects that are no
longer active.

## Repository access

Access to the repositories is provided via <https://> both for anonymous
(read access) and commit access (write access).

**Windows, Linux and macOS libraries**:

- SVN URL:
  <https://svn.blender.org/svnroot/bf-blender/trunk/lib/><operating_system>

**Tests**:

- SVN URL: <https://svn.blender.org/svnroot/bf-blender/trunk/lib/tests>

## Read Access

The source code can be checked out anonymously.

You will need a Subversion client. Most systems have one installed by
default. For a list of clients see [Overview of Subversion
clients](http://subversion.apache.org/packages.html).

When you checkout, you will probably see an error message about the
certificate that is not issued by a trusted authority. However, the
certificate is fully functional and will ensure secure transit of the
data. You can permanently accept this certificate.

If you get the message "Certificate verification error: signed using
insecure algorithm" without proposing to accept the certificate, open
with the file `~/.subversion/servers` and add on the bottom of it:

```
ssl-trust-default-ca = no
```

## Additional information

- [Overview of Subversion clients](http://subversion.apache.org/packages.html)
- [Version Control with Subversion](http://svnbook.red-bean.com/) (online book)
- [Subversion's website](http://subversion.apache.org/)
