# Get in Contact

## Communication Channels

The main ways to get in contact with other developers and contributors are:

- [Blender Chat](../communication/chat.md) for informal conversation and discussions with developers.
- [Developer Forum](https://devtalk.blender.org) for general developer discussion and questions.

Bug reporting, code contribution and review, and design and planning tasks are all done on [projects.blender.org](https://projects.blender.org).

If you want to start developing in Blender you will have lots of startup
questions. You can ask them in the
[\#blender-coders](https://chat.blender.org/#/room/#blender-coders:blender.org) chat
each day of the week, or in the relevant
[module channel](../communication/chat.md#modules).

When in doubt and no particular channel seems to match your question,
write a message on \#blender-coders and you will get an answer there or
get redirected to the right channel.

## Module Meetings

The modules have autonomy to organize their work how they see fit. Some
modules have weekly or bi-weekly meetings. The less active modules meet
once there is something specific to discuss.

The future meetings are announced as part of the
[meeting notes](https://devtalk.blender.org/tag/meeting).

## Contribution Process

If you have found an area in Blender you want to improve on and it's
a non-trivial change, check in advance if Blender developers agree
with the project and design.

This helps avoid doing work for nothing, in case the feature should
have a different design, is already under development, or is not desired
functionality.

The chat and forums are good places to get feedback about this.
To talk to the individual owners of the relevant code, see the
[list of modules and their owners](https://projects.blender.org/blender/blender/wiki).

Once you have written the code, see the
[contributing code](../contributing/index.md) section on how to
[make a pull request](../contributing/pull_requests.md) get it submitted,
reviewed and included.

## Release Process

See the [release cycle](../release_process/release_cycle.md) and
[current projects](../organization/projects.md) to understand the
schedule and times when new features can get in and releases are made.
