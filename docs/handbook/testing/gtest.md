# C/C++ Tests

[GoogleTest](https://google.github.io/googletest/) is the C++ testing
framework used to test C/C++ code in Blender.

Building of tests must be enabled using the `WITH_GTESTS` CMake build
option, tests can then be run with `make test`.

## Test Executables

Unit tests are bundled in one executable per module, for example
`blenlib_test`. Integration tests that depend on other Blender modules
are built as part of the `blender_test` executable.

The tests executables are output to `bin/tests/` in the build
directory.

## Test Source Files

Test code is located in `tests/` folders in modules, next to the
source files for the module.

Details are described in [the Tests section of the C++ style
guide](../guidelines/c_cpp.md#tests).

## Example

The tests for Blender's path utilities:
[`blenlib/intern/path_util.c`](http://developer.blender.org/diffusion/B/browse/master/source/blender/blenlib/intern/path_util.c)

Test file located in:
[`blenlib/tests/BLI_path_util_test.cc`](http://developer.blender.org/diffusion/B/browse/master/source/blender/blenlib/tests/BLI_path_util_test.cc)

The test can be executed from the build directory:
`./bin/tests/blenlib_test`

And produces output, for example:

``` text
[==========] Running 3 tests from 1 test case.
[----------] Global test environment set-up.
[----------] 3 tests from path_util
[ RUN      ] path_util.PathUtilClean
[       OK ] path_util.PathUtilClean (0 ms)
[ RUN      ] path_util.PathUtilFrame
[       OK ] path_util.PathUtilFrame (0 ms)
[ RUN      ] path_util.PathUtilSplitDirfile
[       OK ] path_util.PathUtilSplitDirfile (0 ms)
[----------] 3 tests from path_util (0 ms total)

[----------] Global test environment tear-down
[==========] 3 tests from 1 test case ran. (0 ms total)
[  PASSED  ] 3 tests.
```

## Scaffolding

When testing anything other than pure functions working on primitive data types, there are a
number of prerequisite steps that need to be done at startup for the a test to even run. Examples
of times that this would be needed is anything involving `Depsgraph` evaluation.

The following is a stripped down example of a [TestFixture](https://google.github.io/googletest/primer.html#same-data-multiple-tests)
that can be extended to fit further setup and teardown of test data.

```c++
class ExampleTestFixture : public testing::Test {
  public:
    Main *bmain;
    Scene *scene;

  static void SetUpTestSuite()
  {
    CLG_init(); // Many systems have a hard dependency on logging being available.
    BKE_idtype_init(); // Allows actually adding defined ID blocks (Scene, etc).
    RNA_init();
    BKE_appdir_init(); // Dependency of IMB_init.
    IMB_init(); // Needed for scene initialization.
  }

  void SetUp() override
  {
    bmain = BKE_main_new();
    scene = BKE_scene_add(bmain, "Test Scene");
  }

  void TearDown() override
  {
    BKE_main_free(bmain);
  }

  static void TearDownTestSuite()
  {
    RNA_exit();
    IMB_exit();
    BKE_appdir_exit();
    CLG_exit();
  }
};

TEST_F(ExampleTestFixture, example) {
  EXPECT_TRUE(true);
}
```

## Further Reading

- [List of assertions that can be used in
  tests](https://google.github.io/googletest/reference/assertions.html)
- [Automated
  Testing](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=339&milestone=0&project=0&assignee=0&poster=0)
  issues on projects.blender.org
