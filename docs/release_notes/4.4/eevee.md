# Blender 4.4: EEVEE & Viewport

## 3D Viewport

### Overlays

The overlays have been rewritten for better constancy and better extendability.

This is the cause of small changes in the way overlays are draw:

- Objects origins now always draw on top of edit mode overlays.
- Display in Xray mode now fade in-front overlays correctly.

### User Interface
* Expose view *Lock Rotation* property in the sidebar View panel. (blender/blender@7d98a24e3e)
