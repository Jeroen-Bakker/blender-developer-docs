# Blender 4.4: User Interface

## Asset Browser

- New option to sort assets by asset catalog instead of by name.
  This is the new default sorting method.
  (blender/blender@f49d0390a1b6)
- New operator to remove the preview of an asset. Find it in the Asset Browser sidebar. (blender/blender@40b0185a8e)
- Brush names are easier to read with Light theme. (blender/blender@dcec1d5)

## Files

- Default file names now capitalized as "Untitled". (blender/blender@0ee4ae89e4)
- Full file name now shown on the Recent items tool-tips. (blender/blender@0e83b9c5ee)

## Fonts

- Interface font, "Inter", updated to 4.1. (blender/blender@6696f11808)
- Improved calculation of text string length for monospaced fonts. (blender/blender@7492c7aa6)
- Improved selection from Fonts folder. (blender/blender@7a1e95ea91)

## General

- Tool-tips have improved vertical centering. (blender/blender@8dc8e48e9a)
- Tooltip status colors work better on light backgrounds. (blender/blender@c5bce9d710)
- Color tooltips now propertly show colors that are without alpha. (blender/blender@dbe50711ce)
- Centered dialogs can now be moved without them snapping back. (blender/blender@154ab16ee2)
- Popup informational alerts, like Python script warning, adjust width based on contents. (blender/blender@0add2857a3)
- Menu accelerators (underlined letters) now work with toggles. (blender/blender@ba9417470e)
- Status Bar notification banners are now truncated when very long. (blender/blender@0ba91e9870)
- _Adjust Last Operation_ becomes unavailable after undoable interactions with UI elements. (blender/blender@1bde901bf28e)
- Increased contrast for transform cursors. (blender/blender@26c15292a7)
- Larger alternative version of the "frame" cursor for high-DPI displays. (blender/blender@26c15292a7)
- Batch Rename: Add icons to data type menu (blender/blender@e2e992a425)
- Rename masking property to better reflect its usage (blender/blender@18f145d1ac)

## 3D Viewport

- Default color for front face for Face Orientation overlay changed to transparent.
  (blender/blender@8bcb714b9e)
- Overlays: Mesh indices overlay setting is now always visible, it no longer depends on Developer Extras. (blender/blender@ef2bff2004)
- Notification shown when hiding objects. (blender/blender@bc5fcbe1c3)
- Can delete "Measure" tool items when gizmos are turned off. (blender/blender@3f91350d4e)
- Using Text Objects with a non-default font, missing characters are no longer loaded from other fonts. (blender/blender@6fa5295c1d)
- Using Text Objects with a missing or invalid font, characters no longer used from default font. (blender/blender@5ce10722ab08)
- Collections can now be set to disabled in viewports. (blender/blender@06a2617107)
- Can now play animation while in Sculpt mode. (blender/blender@1debbcae1e)
- Viewport Render Animation now shows a progress bar. (blender/blender@627114ee54c87bb5d9c243849996c3b81346d040)
- Expand brush/tool falloff curve presets in popovers (blender/blender@5e5c68fa62)


## Editors

- Resizing editors now softly snaps to minimum and maximum.
  (blender/blender@62541bffc2)
- Improved rounded edge for splitting preview.
  (blender/blender@c3ea941273)
- Docking operation feedback descriptions improved.
  (blender/blender@09e6bab7bc)
- Minimum size imposed for docking operation creation of new editor.
  (blender/blender@f339ab4325)
- Splitting improvements including soft snapping and minimum size increased.
  (blender/blender@82667d5626)
- Using separate specific mouse cursors for Join operations.
  (blender/blender@798a48f6ef)
- Correct shortcuts now shown when hovering over Editor Change button. (blender/blender@9ed7b03e35)
- Scroll-bar properly hidden for tiny editors. (blender/blender@b846797074)

## Spreadsheet

- The selection filter for meshes now gives expected results for non-vertex domains.
  (blender/blender@76e9dc0f0424)

## Nodes

- The sidebar is see-through when *Region Overlap* is enabled in the Preferences, similar to the 3D Viewport (blender/blender@9b7e660fad).
- Panels in node groups can now be nested (blender/blender@7dc630069b48fa16ce1d4aedc1513dd5bdd6cdb7).
- The name input in the Attribute node in the shader editor is wider now (blender/blender@ea8c45e10a154be22a9f7d47f5cbfb49ab368f84).
- Node title text truncation is improved (blender/blender@5edc68c442, blender/blender@4ff47d85e0).
- Long multicolumn lists in nodes will now collapse to single column when space limited. (blender/blender@fe071cd076)
- Input values that don't affect the output are grayed out now. (blender/blender@80441190c6e04ec2).

## Outliner

- Improved Outliner vertex group sorting. (blender/blender@531ed68061)
- Outliner supports ctrl/shift for excluding collections. (blender/blender@a1fc2eb37a)
- No more overlapping icons in Outliner with some display options. (blender/blender@25febbbb32)
- Can not un-isolate collection when a linked collection is present.
  (blender/blender@f181262634)

## Properties Editor

- Tree-views no longer forget their changed height after hiding/unhiding them,
  or reloading the file (if _Load UI_ is enabled).
  (blender/blender@f0db870822)
- Tree View displays lines correctly when local zoom changes. (blender/blender@5ec2547ab0)
- UI Lists now be sorted in reversed alphabetical. (blender/blender@72a9990779)
- Improved tab key navigation between inputs in the color picker. (blender/blender@93e8c231aa)
- Opening context menu from numerical input now shows correct mouse cursor.
  (blender/blender@e6d941cdf4)
- Custom integer properties no longer have -10000/10000 soft minimum and maximum.
  (blender/blender@9e6c884394)
- Improved container/codec ordering in FFMPEG video drop-downs. (blender/blender!118412)
- Reset to Defaults now works with Camera field of view. (blender/blender@e5e45dc140)
- Mesh Cache modifier "Flip Axis" toggles now work correctly. (blender/blender@d2418b89c3)
  
## Preferences

- More user preferences now reset to actual defaults. (blender/blender@6a06e87f85)
- Language translation options are now preserved when changing languages. (blender/blender@b801615431)
- Preference for UI Icon alpha no longer affects matcaps and studio lights. (blender/blender@2b7a968909)
- Minimum User Scale preference increased to 0.5. (blender/blender@6922418a95)
- Improved Studio Lights Editor interface layout.
  (blender/blender@f6bbcaba6d)
  
![Studio Lights Editor Layout](images/studio_lights_editor_layout.png){ style="width:400px;" }
  
## Animation

- Animated values for nodes, node sockets, and geometry nodes inputs have more useful names in the list (blender/blender@bd8edc7c27513842b083fc0a481a4b9935dd13a6).
- Auto keyframe toggle now works better if assigned to keyboard shortcut. (blender/blender@0e1313483a)
- Tooltips can now show while animation is playing. (blender/blender@a7334db09b)
- Improved feedback during Animation Playback timer test. (blender/blender@df57beb676)
- Improved current frame indicator styling in movie clip and image editors. (blender/blender!132911)

## Platforms

- Windows: Copy and paste OS image paths into Image Editor.
  (blender/blender@0193579e90)
- Windows: File system volume names display correctly with high-bit Unicode characters.
  (blender/blender@fe6609c4eb)
- macOS: Fixed title bar file icon not being cleared when creating a new file.
  (blender/blender@b99497bf21)
- macOS: Improved color picking outside of Blender windows. (blender/blender@60a3f886c6)
- Linux: Fixed text pasting from Blender to certain other applications (such as Firefox) not working under X11.
  (blender/blender@720fc44ca38330bbc1b8cfec5f37b56a2d6b46c3)
