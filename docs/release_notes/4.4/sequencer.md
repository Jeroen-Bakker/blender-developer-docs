# Blender 4.4: Sequencer

## User Interface
- Text strips can be now edited directly in preview area (enter edit mode with Tab key).
  (blender/blender!127239) <br/>
  ![](images/vse_text_editing.png)
- Text strips with word-wrapped or multi-line text can now properly do
  left/center/right alignment within each line.
  (blender/blender!126660) <br/>
  ![](images/vse_text_alignment.png)
- Text strip background fill ("Box") got an option to have rounded
  corners.
  (blender/blender!129665) <br/>
  ![](images/vse_tex_box_round.png)
- Text strip style UI uses collapsible panels for outline/shadow/box.
  (blender/blender!130449)
  ![](images/vse_text_strip_panels.png)
- Snapping now works with retiming keys. Strips can also be snapped to
  retiming keys.
  (blender/blender!129709)
- Strip images can be duplicated in preview area.
  (blender/blender!131529)
- Add effect menu has been organized, with improved polling. Effects may
  now be added to connected strips without erroring if sound strips are
  present in the selection.
  (blender/blender!132672)

## Performance
- Building proxies for image sequences is faster now.
  (blender/blender!128752)
- Preview playback performance of float/HDR content is faster now.
  (blender/blender!128829)
- Text strip background fill ("Box") is several times faster for large
  fill areas.
  (blender/blender!130403)
- Curves, Hue Correct, White Balance modifiers are 1.5x-2x faster now.
  (blender/blender!131736)

## Video
- Videos can now be rendered using H.265/HEVC codec. (blender/blender!129119)
- 10 and 12 bit/channel videos are supported now (blender/blender!129298):
  - When reading 10/12 bit videos, they are read into a floating point image,
  - When rendering, there's an option for 10 or 12 bit color depth for supported
    codecs (10 bit for H.264, H.265, AV1; 12 bit for H.265, AV1).
- Videos rendered from Blender are in BT.709 color-space now (was unspecified
  color-space before, which led to differences in how various players display them).
  (blender/blender!130021)
- Video playback YUV->RGB conversion is more accurate now, fixing color shifts and
  banding in dark regions. (blender/blender!130383)
- Video file display rotation metadata is respected now. If you had worked
  around it previously, you'll need to undo the additional transform rotation now.
  (blender/blender!130455)
 
## Other
- Proxies for EXR (or other float/HDR format) image strips now work properly,
  without losing range/precision. Float image proxies are internally saved
  as EXR files with DWAA compression and 16-bit float data type.
  (blender/blender!128835)
- Copying strips or creating metastrips now automatically includes effect chains
  instead of erroring with a message asking the user to select all related strips.
  (blender/blender!132930)
  
## Python API

Properties that refer to a `bpy.types.Strip` (previously `bpy.types.Sequence`) as "sequence" have been deprecated. New properties with "strip" are added. The old properties remain available until 5.0 but addon developers are encouraged to update their addons to use the new property names:

| Deprecated property | New property |
| --------------------------------- | ----------------------------------- |
| `context.active_sequence_strip` | `context.active_strip` |
| `context.selected_editable_sequences` | `context.selected_editable_strips` |
| `context.selected_sequences` | `context.selected_strips` |
| `context.sequences` | `context.strips` |
| `SequenceEditor.sequences` | `SequenceEditor.strips` |
| `SequenceEditor.sequences_all` |  `SequenceEditor.strips_all` |
| `MetaStrip.sequences` | `MetaStrip.strips` |

### Breaking changes

Text strip `align_x` and `align_y` were renamed to `anchor_x` and `anchor_y`. Property `alignment_x` does proper alignment now. (blender/blender@77a5478c0f5460b3284826c5f11f818c26f9823b)

The type `bpy.types.Sequence` has been renamed to `bpy.types.Strip` together will all related types. (blender/blender@d3ba70190b97778943d08ca93f0e2346d09fa1a5)

Here is the full list:

| `4.3` | `4.4` |
|--------|--------|
|`bpy.types.Sequence`|`bpy.types.Strip`|
|`bpy.types.EffectSequence`|`bpy.types.EffectStrip`|
|`bpy.types.AddSequence`|`bpy.types.AddStrip`|
|`bpy.types.AdjustmentSequence`|`bpy.types.AdjustmentStrip`|
|`bpy.types.AlphaOverSequence`|`bpy.types.AlphaOverStrip`|
|`bpy.types.AlphaUnderSequence`|`bpy.types.AlphaUnderStrip`|
|`bpy.types.ColorMixSequence`|`bpy.types.ColorMixStrip`|
|`bpy.types.ColorSequence`|`bpy.types.ColorStrip`|
|`bpy.types.CrossSequence`|`bpy.types.CrossStrip`|
|`bpy.types.GammaCrossSequence`|`bpy.types.GammaCrossStrip`|
|`bpy.types.GaussianBlurSequence`|`bpy.types.GaussianBlurStrip`|
|`bpy.types.GlowSequence`|`bpy.types.GlowStrip`|
|`bpy.types.MulticamSequence`|`bpy.types.MulticamStrip`|
|`bpy.types.MultiplySequence`|`bpy.types.MultiplyStrip`|
|`bpy.types.OverDropSequence`|`bpy.types.OverDropStrip`|
|`bpy.types.SpeedControlSequence`|`bpy.types.SpeedControlStrip`|
|`bpy.types.SubtractSequence`|`bpy.types.SubtractStrip`|
|`bpy.types.TextSequence`|`bpy.types.TextStrip`|
|`bpy.types.TransformSequence`|`bpy.types.TransformStrip`|
|`bpy.types.WipeSequence`|`bpy.types.WipeStrip`|
|`bpy.types.ImageSequence`|`bpy.types.ImageStrip`|
|`bpy.types.MaskSequence`|`bpy.types.MaskStrip`|
|`bpy.types.MetaSequence`|`bpy.types.MetaStrip`|
|`bpy.types.MovieClipSequence`|`bpy.types.MovieClipStrip`|
|`bpy.types.MovieSequence`|`bpy.types.MovieStrip`|
|`bpy.types.SceneSequence`|`bpy.types.SceneStrip`|
|`bpy.types.SoundSequence`|`bpy.types.SoundStrip`|
|`bpy.types.SequenceColorBalanceData`|`bpy.types.StripColorBalanceData`|
|`bpy.types.SequenceColorBalance`|`bpy.types.StripColorBalance`|
|`bpy.types.SequenceCrop`|`bpy.types.StripCrop`|
|`bpy.types.SequenceElement`|`bpy.types.StripElement`|
|`bpy.types.SequenceElements`|`bpy.types.StripElements`|
|`bpy.types.SequenceModifier`|`bpy.types.StripModifier`|
|`bpy.types.SequenceModifiers`|`bpy.types.StripModifiers`|
|`bpy.types.SequenceProxy`|`bpy.types.StripProxy`|
|`bpy.types.SequenceTransform`|`bpy.types.StripTransform`|
|`bpy.types.SequencesMeta`|`bpy.types.StripsMeta`|
|`bpy.types.SequencesTopLevel`|`bpy.types.StripsTopLevel`|
  