# Blender 4.4: Animation & Rigging

## Slotted Actions

![The icon for Action Slots](images/action_slot.svg){style="width:10rem;"}

The structure of [Actions](https://docs.blender.org/manual/en/4.4/animation/actions.html#actions)
has changed, introducing the concept of
[Slots](https://docs.blender.org/manual/en/4.4/animation/actions.html#action-slots)
(blender/blender@43d7558e5b00a14b39e473aec345e5080a5e76d2).
In short, slots make it possible to store the animation of multiple "things" into the same Action.

![Action editor, showing one Action with two Slots,
one for the Camera Object and one for the Camera Data.
You can distinguish them by the icon on the right-hand side of the channel list.
The camera is both moving and zooming in.](images/anim_slotted_actions_action_editor.png)

Examples of slotted Actions use:

- Mesh objects: location in the scene (object), together with shape key values
  (belongs to the object data), and the locations of 'hook' empties (different objects).
- Camera objects: transform (object) and lens parameters (object data).
- Meta-ball transforms, where all objects in a group of meta-balls can share the same Action.
- Materials, which effectively consist of two data-blocks
  (the Material itself, and its shader node tree).

As a result of this change:

- All newly created Actions will be slotted Actions.
- Actions loaded from disk will be versioned to slotted Actions.
- A new Python API for slots, layers, strips, and channel bags is available.
- A legacy Python API for accessing F-Curves and Action Groups is still available, and will operate
  on the F-Curves/Groups for the first slot only.
- Creating an Action by keying (via the UI, operators, or the `rna_struct.keyframe_insert` function)
  will try and share Actions between related data-blocks. See [#126655: Anim: Reuse action between
  related data](https://projects.blender.org/blender/blender/pulls/126655) for more info about this.
- Assigning an Action to a data-block will auto-assign a suitable Action Slot. The logic for this is
  described below. However, There are cases where this does _not_ automatically assign a slot, and
  thus the Action will effectively _not_ animate the data-block. Effort has been spent to make
  Action selection work both reliably for Blender users as well as keep the behavior the same for
  Python scripts. Where these two goals did not converge, reliability and understandability for
  users was prioritized.

Auto-selection of the Action Slot upon assigning the Action works as follows. The first rule to find
a slot wins.

1. The data-block remembers the slot name that was last assigned. If the newly assigned Action has a
    slot with that name, it is chosen.
2. If the Action has a slot with the same name as the data-block, it is chosen.
3. If the Action has only one slot, and it has never been assigned to anything, it is chosen.
4. If the Action is assigned to an NLA strip or an Action constraint, and the Action has a single
    slot, and that slot has a suitable ID type, it is chosen.

This last step is what I was referring to with "Where these two goals did not converge, reliability
and understandability for users was prioritized." For regular Action assignments (like via the
Action selectors in the Properties editor) this rule doesn't apply, even though with legacy Actions
the final state ("it is animated by this Action") differs from the final state with slotted Actions
("it has no slot so is not animated"). This is done to support the following workflow:

- Create an Action by animating Cube.
- In order to animate Suzanne with that same Action, assign the Action to Suzanne.
- Start keying Suzanne. This auto-creates and auto-assigns a new slot for Suzanne.

If rule 4. above would apply in this case, the 2nd step would automatically select the Cube slot for
Suzanne as well, which would immediately overwrite Suzanne's properties with the Cube animation.

Note that 'slotted Actions' and 'layered Actions' are the exact same thing, just focusing on
different aspects (slot & layers) of the new data model.

The following limitations are in place:
- an Action can have zero or one layer,
- that layer can have zero or one strip,
- that strip must be of type 'keyframe' and be infinite with zero offset.

For more information on slotted Actions, see [Multi Data-Block
Animation](../../features/animation/animation_system/layered.md#phase-1-multi-data-block-animation).


### Working with Slotted Actions

**Merging Animation**

From the Action Editor it is possible to merge the animation of all selected objects into a single Action 
(<span class="literal">Action</span> » <span class="literal">Merge Animation</span>).
The data is always merged into the Action of the active object. This functionality will look into related
datablocks of each selected object and merge those too. That means it is possible to merge e.g. Object
and Shapekey animation of even a single object. (blender/blender@34a18c7608, [Manual](https://docs.blender.org/manual/en/4.4/editors/dope_sheet/modes/action.html#action-menu))

**Separating Animation**

Also from the Action Editor it is possible to move Slots selected in the Channels Region into a new Action 
(<span class="literal">Action</span> » <span class="literal">Move Slots to new Action</span>).
All Slots moved that way will be moved into a single Action. Users of those Slots will be
re-assigned to the new Action. (blender/blender@95b6cf0099, [Manual](https://docs.blender.org/manual/en/4.4/editors/dope_sheet/modes/action.html#action-menu))

It is also possible to separate each Slot into its own Action 
(<span class="literal">Action</span> » <span class="literal">Separate Slots</span>).
As with moving specific Slots out of the Action, 
this will re-assign all users of those Slots to the new Action.
(blender/blender@85e4bd19f6, [Manual](https://docs.blender.org/manual/en/4.4/editors/dope_sheet/modes/action.html#action-menu))


## Rigging

- Bone collection membership is now mirrored when symmetrizing an armature. For details
  [see the Manual](https://docs.blender.org/manual/en/latest/animation/armatures/bones/editing/symmetrize.html).
  (blender/blender@16c2e71ab0)


## Graph Editor

- The algorithm for generating the values of the F-Curve Noise Modifer has been updated. This was in response
  to a [bug report](https://projects.blender.org/blender/blender/issues/123875).
  Now the values don't exceed the -0.5/0.5 range anymore. Doing so added two new
  properties that control the look of the noise: Lacunarity and Roughness.
  The default values are chosen to closely resemble the previous behavior.
  There is a checkbox to use the old noise,
  which old files will automatically have enabled to not change the look of the scene.
  (blender/blender@2536ddac0d,
  [Manual](https://docs.blender.org/manual/en/latest/editors/graph_editor/fcurves/modifiers.html#noise-modifier))


## Constraints

- Relationship Lines for constraints (between the constrained object/bone and the constraint target) are no longer drawn when there is no target (blender/blender@266d1e8d2fdd2cc821fa5993d983c0893e5fc428). Previously these lines were drawn to the world origin when there was no target.
