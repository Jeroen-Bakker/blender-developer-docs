# Blender 4.4: Sculpt, Paint, Texture

## Brushes
* Cloth brushes now have the *Persistent* option off by default. (blender/blender@8b853e46a06c2c31c79d7f9cdb41f08fe518eee2)
* *Grab Random Cloth* and *Grab Cloth* now use *Local* *Simulation Area* by default. (blender/blender@7ba732a9119396ee703f724717a8618dcddce6a6)

## Operators
* Entering Sculpt Mode via the `sculpt.sculptmode_toggle` operator is prevented for invisible objects. (blender/blender@8d9bf47ba6edc770afd70d77f649d06dc1de649e)
* The *Rebuild BVH* (`sculpt.optimize`) operator no longer adds an undo entry. (blender/blender@a9f127975f835bc0709c5ffb786e4919344dd928)
* *Frame Selected* renamed to *Frame Last Stroke* inside Sculpt, Vertex Paint, Weight Paint, and Texture Paint. (blender/blender@9e78f1c840874671e5f535a9a773ce9eff054f3e)
* `sculpt.brush_stroke`, `paint.vertex_paint`, and `paint.weight_paint` operators now have a `override_location` property to force the operators to calculate stroke positions based off of the provided `mouse_event` values. (blender/blender@baffd174c8303728aae4c56caab0294af6b84b1f)

## Keymap
* The `paint.sample_color` operator has a new default keybind (Shift + Ctrl + X) in Texture Paint to sample the merged color. (blender/blender@0c1ee9eba2978af439c225d4fa8800dcdeda6692)