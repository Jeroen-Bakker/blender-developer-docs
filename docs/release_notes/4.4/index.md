# Blender 4.4 Release Notes

Blender 4.4 is currently in **Alpha** until February 5,
2025.
[See schedule](https://projects.blender.org/blender/blender/milestone/24).

Under development in [`main`](https://projects.blender.org/blender/blender/src/branch/main).

* [Animation & Rigging](animation_rigging.md)
* [Compositor](compositor.md)
* [Core](core.md)
* [Cycles](cycles.md)
* [EEVEE & Viewport](eevee.md)
* [Geometry Nodes](geometry_nodes.md)
* [Grease Pencil](grease_pencil.md)
* [Modeling & UV](modeling.md)
* [Pipeline, Assets & I/O](pipeline_assets_io.md)
* [Python API & Text Editor](python_api.md)
* [Rendering](rendering.md)
* [Sculpt, Paint, Texture](sculpt.md)
* [User Interface](user_interface.md)
* [Video Sequencer](sequencer.md)

## Compatibility
### Cycles

* On Linux, the minimum ROCm version required to use HIP on AMD GPUs has increased to 6.0.
(blender/blender@4e5a9c5dfb582b03d6e11d751e5a5c7e1bcdda9d)

### Python API
* When defining custom `__new__`/`__init__` constructors in classes inheriting from Blender-defined types, calling the parent matching function [is now mandatory](python_api.md#blender-based-python-classes-construction).
* The `bpy.types.Sequence` and all related types got renamed to `bpy.types.Strip`. See the full list [here](../4.4/sequencer.md#breaking-changes).

<!--
Uncomment sections once there is a first entry in that sections
* [Extensions](extensions.md)
* [Grease Pencil](grease_pencil.md)
* [Sculpt, Paint, Texture](sculpt.md)



-->


