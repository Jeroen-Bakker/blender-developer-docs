# Blender 4.4: Grease Pencil

## Draw Mode

* Added "Stroke placements" back (from 4.2) (blender/blender@cde64dbdd5df60885407850d6cc4ce18a70dfefa)

## Operators (that were missing in 4.3)

* Added the "Fixed", "Sample", and "Merge" modes from the "Simplify" operator back (from 4.2)
  (blender/blender@8fb12839f254a4331ed24eee84b6241bd751af87)
* Added the "Set Start Point" operator back (from 4.2) (blender/blender@46cd7afcda3c58b27243f28a1d7f9be93fea4970)
* Added the "Lasso/Box" erase operators back (from 4.2) (blender/blender@612be514c63d8a5edb70b412f5e40839063a2516)

## Python API

* Added a setter for the `drawing` property on frame python API
  (blender/blender@dc6e879d0f45e623151398e34f9a7cc08cb70296).
  * Can be used to copy a drawing from elsewhere (e.g. another layer or object):
    `frame.drawing = other_drawing`.
* Added a new property `is_expanded` to layer groups
  (blender/blender@e5bdfd533bcc7beb1e38d30432492357bd015dcf).
  This returns `True` if the layer group is expanded in the layer tree UI.
* Added a function to reorder strokes in a drawing: `drawing.reorder_strokes(new_indices=[...])`
  (blender/blender@a265b591bede2a6cf9cda189bee641cbbe94a5a9).
* Added a `color_tag` enum property to layer groups that exposes the color tag (blender/blender@36d69e8491de1002458bc33994e7516e20fa0ebd).
* The influence vertex group was removed from the Texture Mapping modifier (blender/blender@c452d5d9e807243ade36809b61673f590bdf67f7)
