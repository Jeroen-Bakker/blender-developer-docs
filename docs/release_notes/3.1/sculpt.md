# Blender 3.1: Sculpt, Paint, Texture

## UDIM

Tiled images now support file names containing UDIM substitution tokens
([\#92696](http://developer.blender.org/T92696),
[D13057](http://developer.blender.org/D13057),
blender/blender@180b66ae8a1)

The following 2 tokens are supported and can be used while loading and
saving images:

- <UDIM> : A 4-digit notation calculated as `1001 + u-tile + v-tile * 10`
- <UVTILE> : A notation defined as `u(u-tile + 1)_v(v-tile + 1)`

Example: `monster-basecolor.<UDIM>.png` will load/save files like
`monster-basecolor.1021.png` etc.

Example: `monster-basecolor.<UVTILE>.png` will load/save files like
`monster-basecolor.u1_v3.png` etc. (Tiles from `u1_v1` through `u10_v100`
inclusive are supported.)
