# Geometry Nodes

Geometry nodes has been extended with large performance improvements,
more possibilities with many new mesh nodes, and various other utilities
and features.

In **bold** you can find the names of 19 newly added nodes.

### Performance Improvements

- The *Set Position* node has been optimized for meshes, with up to a 4x
  improvement for a simple field
  (blender/blender@602ecbdf9aef58).
- The evaluation system used for fields has been improved in many ways
  - In large fields with many nested values, memory usage can be reduced
    by up to 100x, which can also improve performance by 3-4x
    (blender/blender@1686979747c3b5).
  - Improvements to multi-threading with medium loads (around 10,000
    elements) can improve field evaluation performance by up to 10x
    (blender/blender@658fd8df0bd2427c).
  - Memory use and performance have been improved, especially when few
    elements are processed by many nodes, up to a 20% improvement in
    some cases
    (blender/blender@92237f11eba44d).
  - Processing single values with field nodes is more efficient now,
    which can make a 2-3x difference in very extreme situations
    (blender/blender@47276b84701727c2f18).
- The *Realize Instances* node is now multi-threaded, which can give at
  least a 4x improvement in some common cases
  (blender/blender@f5ce243a56a22d).
- Mesh vertex and face normals have been refactored, improving access
  speed in geometry nodes by up to 40%
  (blender/blender@cfa53e0fbeed71).
  - Other areas like normal calculation, primitive nodes, and more
    complex files also have significant improvements.
- Domain interpolation now only calculates necessary values, for some
  interpolations to faces and face corners
  (blender/blender@796e9d442cf8f9).
  - When selections are used, the improvements can be significant; a 6x
    improvement was measured using the position attribute to a single
    face.
- Displaying large node trees with many nodes in the node editor can be
  almost twice as fast
  (blender/blender@012917837649676e9).
- The *Set Spline Type* node is now multi-threaded, making it faster
  when there are many splines
  (blender/blender@d2f4fb68f5d1747d1827).
- The *Cube* mesh primitive node can be about 75% faster at high
  resolutions
  (blender/blender@aa6c922d99f7ae170).
- The *Grid* mesh primitive is now multi-threaded, with a 2.5x
  performance improvement observed on a 4096x4096 vertex grid
  (blender/blender@cb96435047506c).
- The internals of the *Bounding Box* node have been multi-threaded for
  meshes and point clouds, with observed 4.4x and 5x performance
  improvements
  (blender/blender@6a71b2af66cf10,
  blender/blender@6d7dbdbb44f379).
- The overhead for multi-threading a series of nodes has been lowered,
  for a few percent improvement in some cases
  (blender/blender@e206a0ae960c2c62d).

![All of the new geometry nodes added in Blender 3.1](../../images/New_Geometry_Nodes_3.1.png){style="width:750px;"}

### General

- The result of a field at a certain index can be calculated with the
  **Field at Index** node
  (blender/blender@b88a37a490fa9e).
- The new **Accumulate Field** node is useful as a building block for
  more complex setups, building sums of values inside multiple groups
  (blender/blender@a836ded9902d67).
- It's no longer necessary to add a `#frame` driver to get the scene's
  current animation time, the **Scene Time** node does that instead
  (blender/blender@bd3bd776c8938d).
- The *Compare Floats* node has been upgraded to a generalized
  **Compare** node
  (blender/blender@17578408434faf).
  - The new node can compare most data types, not just floats, even
    strings.
  - Special modes are included for 3D Vectors, to compare directions dot
    products, lengths, and more.
- Close points on meshes and point clouds can be merged with the **Merge
  by Distance** node
  (blender/blender@ec1b0c2014a8b9,
  blender/blender@0ec94d5359d732).
- The *Map Range* node now has a vector data type option
  (blender/blender@5b61737a8f4168).
- The *Boolean Math* node has been expanded with more operations
  (blender/blender@14f6afb0900390).

### Attributes

- The **Domain Size** node gives the size of any attribute domain, like
  the number of vertices in a mesh
  (blender/blender@35124acd197372).
- The *Attribute Statistic* node now has a selection input
  (blender/blender@1a833dbdb9c4ec).
- To make naming more intuitive, the *Transfer Attribute* node geometry
  input has been renamed from "Target" to "Source"
  (blender/blender@6a16a9e661f134).
- An new operator on meshes allows converting generic attributes to UV
  layers or vertex groups, or other generic data types
  (blender/blender@f6888b530ac81a).
  - The operator also makes it possible to change the domain of an
    attribute on original geometry.
  - *The operator only works on original geometry, so the geometry nodes modifier must be applied*

### Instances

- Instances now support dynamic attributes
  (blender/blender@97533eede444217bd28,
  blender/blender@15ecd47b9685dffdf59,
  blender/blender@ba8dd0f24ff16433c).
  - Attributes are propagated in the *Realize instances* node, including
    special handling for the `id` attribute
    (blender/blender@f5ce243a56a22d).
  - Instances attributes are propagated in the *Instance on Points* and
    *Instances to Points* nodes
    (blender/blender@221b7b27fce393fb3d,
    blender/blender@386b112f7652f321f2f).
  - *Geometry Nodes instance attributes are not usable in shader nodes
    in 3.1.*
- Real geometry can be converted to an instance with the **Geometry to
  Instance** node
  (blender/blender@565b33c0ad3196).
- Instances can be removed directly in the *Delete Geometry* node
  (blender/blender@a94d80716e688a).

![Abandoned House Generator by Sozap](../../images/Sozap_Abandoned_House_Generator.jpg)

### Meshes

- The **Dual Mesh** node transforms a mesh's faces into vertices and
  vertices into faces
  (blender/blender@d54a08c8af1251).
- The **Extrude Mesh** node makes it possible to extrude vertices,
  edges, and faces
  (blender/blender@95981c98764832).
- Various nodes have been added to give basic access to mesh topology
  information.
  - **Vertex Neighbors** gives access to the number of vertices and
    faces connected to each vertex
    (blender/blender@2814740f5be86f).
  - **Face Neighbors** gives access to each face's vertex count and the
    number of faces connected by an edge
    (blender/blender@2814740f5be86f).
  - **Edge Vertices** gives access to edge vertex indices and their
    positions
    (blender/blender@2814740f5be86f).
  - **Edge Neighbors** tells how many faces use each edge
    (blender/blender@c4cee2e22180e8).
- More new nodes give useful data derived from meshes.
  - The **Face Area** node provides access to the surface area of each
    of a mesh's faces
    (blender/blender@2814740f5be86f).
  - The **Mesh Island** node outputs a separate index for each piece of
    connected vertices in a mesh
    (blender/blender@069d63561a4c,
    blender/blender@4251455dcd739b).
    - There is also an "Island Count" output for the total number of
      islands in the mesh
      (blender/blender@87c5423c5e1e7b).
  - The **Edge Angle** node gives the angle between the normals of two
    manifold faces
    (blender/blender@b7ad58b945c377).
    - The node can output the signed angle, to tell when an edge is
      convex or concave
      (blender/blender@4d5c08b9387c5d).
- The **Flip Faces** node reverses the normals of selected faces by
  reversing the winding order of their vertices and edges
  (blender/blender@c39d514a4eacd4).
- The **Scale Elements** node changes the size of selected groups of
  faces or edges
  (blender/blender@d034b85f332537).
- The *Triangulate* node now has a selection input to limit the
  operation to only some faces
  (blender/blender@abf30007abdac2).

### Curves

- The *String to Curves* node has two new outputs
  (blender/blender@0e86c60c28b62f):
  - "Line" contains the index of the line in the text layout for every
    character instance.
  - "Pivot Point" gives a position relative to the bounding box of each
    character.
- The *Resample Curve* node now has a selection input for only affecting
  certain splines
  (blender/blender@a7672caeb255e3).
- The *Curve Parameter* node is renamed to *Spline Parameter* and now
  has a length output in addition to "Factor"
  (blender/blender@0c6b8158554463).
- The *Set Handle Position* node has a new "Offset" input like the *Set
  Position* node
  (blender/blender@3fe735d371f4acd6).
- The number of control points in a spline is now included in the
  *Spline Length* node
  (blender/blender@88e9e97ee907b8).
- The *Set Spline Type* node has a better conversion from NURBS to
  Bézier splines
  (blender/blender@a84621347d93bc).
- The *Curve Handle Positions* node now has a toggle to output the
  position relative to the corresponding control point
  (blender/blender@38c7378949b332).

### Primitives

- A new **Arc** curve primitive is similar to the *Curve Circle*
  primitive, but can create an incomplete circle
  (blender/blender@cc1a48e39514a3).
- The *Cone* and *Cylinder* mesh primitives now have selection outputs
  for the top, side, and bottom sections
  (blender/blender@431524aebc5aa9).
- The *Star* curve primitive node has a selection output for the outer
  points
  (blender/blender@3fe735d371f4).

### Spreadsheet Editor

- Volume grids are now listed in the spreadsheet, including the grid
  name, data type, and class
  (blender/blender@ccead2ed9c6121).

# Node Editor

The nodes editor has a new context-aware search menu when dragging
links, and significant performance improvements for large node trees,
with fewer unnecessary updates than before.

![The node timings overlay shows approximately how long each node took to execute.](../../images/Node_Timings_Overlay.png){style="width:500px;"}

- When dragging a node link over empty space, a new search menu allows
  quickly connecting a node with a compatible socket
  (blender/blender@11be151d58ec0c).
- The execution time for nodes can be displayed above nodes (Geometry
  nodes only)
  (blender/blender@e4986f92f32b096a).
- Performance for large node trees has been improved, and unnecessary
  updates have been removed.
  - Editing a node tree causes fewer unnecessary updates than before
    (blender/blender@7e712b2d6a0d25).
    - Inserting reroutes and changing nodes that aren't connected to the
      output don't trigger updates.
    - *Some unnecessary updates still happen when a driver is used.*
  - Displaying large node trees with many nodes in the node editor can
    be almost twice as fast
    (blender/blender@012917837649676e9).
  - Editing and undo with large node trees should be generally faster
    (blender/blender@dbbf0e7f66524a,
    blender/blender@bdbd0cffda697e).
- Node selection inside Frame nodes has been fixed/improved
  (blender/blender@1995aae6e3bf).
