# Blender 4.2 LTS: Color Management

## Khronos PBR Neutral

The Khronos PBR Neutral view transform has been added. (blender/blender@d1cbb10d17)

This tone mapper was designed specifically for PBR color accuracy, to get sRGB
colors in the output render that match as faithfully as possible the input sRGB
base color in materials, under gray-scale lighting. This is aimed toward product
photography use cases, where the scene is well-exposed and HDR color values are
mostly restricted to small specular highlights.

|Standard|AgX|Khronos PBR Neutral|
|-|-|-|
| ![](./images/cm_einar_standard.jpg) | ![](./images/cm_einar_agx.jpg) |![](./images/cm_einar_pbr_neutral.jpg) |
| ![](./images/cm_street_scene_standard.jpg) | ![](./images/cm_street_scene_agx.jpg) |![](./images/cm_street_scene_pbr_neutral.jpg) |
| ![](./images/cm_blender_dvd_case_standard.jpg) | ![](./images/cm_blender_dvd_case_agx.jpg) |![](./images/cm_blender_dvd_case_pbr_neutral.jpg) |

More details and theory behind this mapper is available in the Tone Mapping Considerations for Physically-Based Rendering [article](https://modelviewer.dev/examples/tone-mapping).