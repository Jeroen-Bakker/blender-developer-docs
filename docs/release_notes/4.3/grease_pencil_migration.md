# Grease Pencil migration from older versions to Blender 4.3

With Grease Pencil completely rewritten in 4.3, it is expected that some features work differently.
This page explains the migration process for users as well as add-on developers.

## Screen Space Stroke Thickness

In Blender 4.3, the `Screen Space` stroke thickness option has been removed from the Grease Pencil object-data properties. In order to get the same effect, a geometry nodes modifier can be used to scale the points procedurally to match a specific thickness in pixels in the render.

A basic setup can be downloaded [here](files/screen-space-thickness-nodegroup.blend).

Go to `File`>`Append` navigate to the file and append the "Screen Space Thickness" node group into your file, then add a Geometry Nodes modifier to the Grease Pencil object and select the "Screen Space Thickness" group from the node group selector. Enter the `Camera Data` information of the camera to be used. Optionally, it's also possible to add drivers to these fields to make sure they match the settings of the scene and camera. 

## Python API

Add-ons written using the Grease Pencil python API before 4.3
are expected to break in this version.
This section is meant to help add-on developers update their add-ons for Blender 4.3.

### Grease Pencil Object and ID

In 4.3, the Grease Pencil object has a new type. The following types and modes need updating:

|                   | Blender 4.2              | Blender 4.3                |
| ----------------- | ------------------------ | -------------------------- |
| Object type       | `GPENCIL`                | `GREASEPENCIL`             |
| RNA ID type       | `bpy.types.GreasePencil` | `bpy.types.GreasePencilv3` |
| Object modes      | `*_GPENCIL`              | `*_GREASE_PENCIL`          |
|                   | `EDIT_GPENCIL`           | `EDIT`                     |
|                   | `PAINT_GPENCIL`          | `PAINT_GREASE_PENCIL`      |
|                   | `SCULPT_GPENCIL`         | `SCULPT_GREASE_PENCIL`     |
|                   | `VERTEX_GPENCIL`         | `VERTEX_GREASE_PENCIL`     |
|                   | `WEIGHT_GPENCIL`         | `WEIGHT_GREASE_PENCIL`     |
| Context modes     | `*_GPENCIL`              | `*_GREASE_PENCIL`          |

### Grease Pencil Data

Most of the API for the Grease Pencil data works in the same way, but there are some differences:

#### Moved

|               | Blender 4.2     | Blender 4.3                                           |
| ------------- | --------------- | ----------------------------------------------------- |
| Multi editing | `use_multiedit` | `tool_settings.use_grease_pencil_multi_frame_editing` |
| Grid          | `grid`          | `context.space_data.overlay.gpencil_grid*`            |
| Zdepth Offset | `zdepth_offset` | `tool_settings.gpencil_surface_offset`                |

|                    | Blender 4.2              | Blender 4.3                            |
| ------------------ | ------------------------ | -------------------------------------- |
| Active layer index | `layer.active_index`     | `list(layers).index(layers.active)`    |
| Active frame       | `layer.active_frame`     | `layer.current_frame()`                |
| Layer name         | `layer.info`             | `layer.name`                           |
| Is parented        | `layer.is_parented`      | `layer.parent` (`is not None`)         |
| Line Change        | `layer.line_change`      | `layer.radius_offset`                  |
| Location           | `layer.location`         | `layer.translation`                    |
| Use Mask Layer     | `layer.use_mask_layer`   | `layer.use_masks`                      |

#### Removed

|                  | Blender 4.2               | Blender 4.3                                           |
| ---------------- | ------------------------- | ----------------------------------------------------- |
| Curve Editing    | `use_curve_edit`          | Curve editing is no longer a "mode". All curve types can be edited in edit mode. |
|                  | `curve_edit_corner_angle` |                                                       |
|                  | `curve_edit_threshold`    |                                                       |
|                  | `edit_curve_resolution`   |                                                       |
|                  | `use_adaptive_curve_resolution` |                                                 |
| Is Annotation    | `is_annotation`           | Annotations are a different data-block.               |
| Data Modes       | `is_stroke_*_mode`        | See `object.mode`.                                    |
| Pixel Factor     | `pixel_factor`            | This is converted into a thickness modifier.          |
| Stroke Thickness | `stroke_thickness_space`  | Always `WORLDSPACE`. See [section above](#screen-space-stroke-thickness). |

|                  | Blender 4.2               | Blender 4.3                                           |
| ---------------- | ------------------------- | ----------------------------------------------------- |
| Active Note      | `layers.active_note`      |                                                       |
| Annotation layer settings | `layer.annotation_*` |                                                   |
| Layer is ruler   | `layer.is_ruler`          |                                                       |
| Layer Stroke Color | `layer.color`           |                                                       |
| Layer Parent Type | `layer.parent_type`      | Use `layer.parent.type`.                              |
| Show Points      | `layer.show_points`       |                                                       |
| Annotation thickness | `layer.thickness`     |                                                       |
| Annotation onion skinning | `layer.use_annotation_onion_skinning` |                                  |

### Grease Pencil Strokes & Points

In 4.2 strokes and points were accessible through the `frame.strokes` and `strokes.points` properties.
In 4.3, the `frame` references a `drawing` that contains all the data of the strokes and points.
All of this data is stored as [attributes](https://docs.blender.org/api/4.3/bpy.types.Attribute.html#attribute-bpy-struct) (`drawing.attributes`) on the `CURVE` and `POINT` [domain](https://docs.blender.org/manual/en/4.3/modeling/geometry_nodes/attributes_reference.html#attribute-domains).

#### Compatibility API

To ease the transition to attributes and to give python developers a higher-level API, there is a
**compatibility API** (`drawing.strokes`) that works almost the same as the `frame.strokes` API in 4.2.
This API is implemented in python.
See [GreasePencilDrawing](https://projects.blender.org/blender/blender/src/commit/0bc595bbabf74873aa33133b7e3702007fec3a44/scripts/modules/bpy_types.py#L1405) and [GreasePencilStrokeSlice](https://projects.blender.org/blender/blender/src/commit/0bc595bbabf74873aa33133b7e3702007fec3a44/scripts/modules/_bpy_internal/grease_pencil/stroke.py#L294) for the code.

It is important to note that while the compatibility API provides almost the same functionality, it doesn't work the same way as in 4.2:

* `GreasePencilStroke`s and `GreasePencilStrokePoint`s are **not** RNA types, only helper classes. They are essentially a reference to the `drawing` + index of the stroke/point. This means that storing a reference to a `GreasePencilStroke` or a `GreasePencilStrokePoint`, then applying some change to the drawing, and accessing the reference again will result in undefined behavior. See the following example:
  ```py
  stroke = drawing.strokes[0].points[10]
  drawing.add_strokes(...)
  drawing.resize_strokes(...)
  stroke.cyclic # Undefined behavior. Drawing changed.
  ```
* The performance of the compatibility API is generally expected to be worse than the API in 4.2. For performance critical applications, use the `drawing.attributes` API. See the [Attributes documentation](https://docs.blender.org/api/4.3/bpy.types.Attribute.html#using-attributes) for more info.

While the `drawing.strokes` API is similar to the `frame.strokes` API, it has some differences. See the following table.

#### Moved

|                    | Blender 4.2                | Blender 4.3                            |
| ------------------ | -------------------------- | -------------------------------------- |
| Move Frame         | `frame.frame_number = 42`  | `frames.move(frame.frame_number, 42)`  |
| Strokes API        | `frame.strokes`            | `frame.drawing.strokes`                |
| Add strokes        | `strokes.new()`            | `drawing.add_strokes(sizes)`           |
| Remove strokes     | `strokes.remove(stroke)`   | `drawing.remove_strokes(indices=(0,))` |
| Stroke Aspect Ratio| `stroke.aspect`            | `stroke.aspect_ratio`                  |
| Stroke Start Cap   | `stroke.start_cap_mode`    | `stroke.start_cap`                     |
| Stroke End Cap     | `stroke.end_cap_mode`      | `stroke.end_cap`                       |
| Stroke Hardness    | `stroke.hardness`          | `1.0 - stroke.softness`                |
| Stroke Fill Color  | `stroke.vertex_color_fill` | `stroke.fill_color`/`stroke.fill_opacity` |
| Stroke cyclic      | `stroke.use_cyclic`        | `stroke.cyclic`                        |
| Add stroke points  | `stroke.add()`             | `stroke.add_points()`                  |
| Remove stroke points | `stroke.pop()`           | `stroke.remove_points()`               |
| Point coordinate   | `point.co`                 | `point.position`                       |
| Point pressure     | `point.pressure`           | `point.radius` (=`stroke.thickness * point.pressure`) |
| Point strength     | `point.strength`           | `point.opacity`                        |
| Point uv rotation  | `point.uv_rotation`        | `point.rotation`                       |
| Point time         | `point.time`               | `point.delta_time`                     |

#### Removed

|                     | Blender 4.2              | Blender 4.3                            |
| ------------------- | ------------------------ | -------------------------------------- |
| Frame is edited     | `frame.is_edited`        |                                        |
| Close stroke        | `strokes.close(stroke)`  |                                        |
| Stroke Bounding Box | `stroke.bounds_*`        |                                        |
| Stroke Display Mode | `stroke.display_mode`    |                                        |
| Stroke Edit Curve   | `stroke.edit_curve`      | Stroke can use the `BEZIER` type.      |
|                     | `stroke.has_edit_curve`  |                                        |
|                     | `stroke.is_nofill_stroke`|                                        |
| Stroke Thickness    | `stroke.line_width`      | Thickness combined into point radii    |
| Stroke selection index | `stroke.select_index` |                                        |
| Triangles           | `stroke.triangles`       |                                        |
| Points update       | `points.update()`        |                                        |
| Point uv factor     | `point.uv_factor`        |                                        |

### Modifiers

Grease Pencil modifiers are now regular modifiers. 

|                   | Blender 4.2                      | Blender 4.3                   |
| ----------------- | -------------------------------- | ----------------------------- |
| Modifiers         | `object.grease_pencil_modifiers` | `object.modifiers`            |
| Modifier ID type  | `bpy.types.GpencilModifier`      | `bpy.types.Modifier`          |
| Modifier type     | `GP_*`                           | `GREASE_PENCIL_*`             |

Grease Pencil modifier property name changes

|                                  | Blender 4.2          | Blender 4.3                |
| -------------------------------- | -------------------- | -------------------------- |
| Opacity Modifier -> Factor       | `factor`             | `color_factor`             |

### Operators

|                   | Blender 4.2                      | Blender 4.3                   |
| ----------------- | -------------------------------- | ----------------------------- |
| Operators         | `bpy.ops.gpencil.*`              | `bpy.ops.grease_pencil.*`     |

#### Moved 

|                           | Blender 4.2                         | Blender 4.3                            |
| ------------------------- | ----------------------------------- | -------------------------------------- |
| Active frame delete all   | `active_frames_delete_all()`        | `active_frame_delete(all=True)`        |
| Insert blank frame        | `blank_frame_add()`                 | `insert_blank_frame()`                 |
| Convert                   | `convert()`                         | `object.convert()`                     |
| Draw stroke               | `draw()`                            | `brush_stroke()`                       |
| Edit mode toggle          | `gpencil.editmode_toggle()`         | `object.mode_set('EDIT', toggle=True)` |
| Generate weights          | `generate_weights()`                | Happens when calling `object.parent_set()` with armature |
| Hide layers               | `hide()`                            | `layer_hide()`                         |
| Layer mask move           | `layer_mask_move()`                 | `layer_mask_reorder()`                 |
| Material copy to object   | `materials_copy_to_object()`        | `material_copy_to_object()`            |
| Reset transform fill      | `reset_transform_fill()`            | `reset_uvs()`                          |
| Unhide all layers         | `reveal()`                          | `layer_reveal()`                       |
| Select                    | `select()`                          | `view3d.select()`                      |
| Select Box                | `select_box()`                      | `view3d.select_box()`                  |
| Select Circle             | `select_circle()`                   | `view3d.select_circle()`               |
| Select Lasso              | `select_lasso()`                    | `view3d.select_lasso()`                |
| Select First              | `select_first()`                    | `select_ends(amount_start=1)`          |
| Select Last               | `select_last()`                     | `select_ends(amount_end=1)`            |
| Select Grouped            | `select_grouped()`                  | `select_similar(mode)`                 |
| Select vertex color       | `select_vertex_color()`             | `select_similar(mode='VERTEX_COLOR')`  |
| Arrange strokes           | `stroke_arrange()`                  | `reorder()`                            |
| Set stroke caps           | `stroke_caps_set()`                 | `caps_set()`                           |
| Set stroke material       | `stroke_change_color()`             | `set_material()`                       |
| Set stroke cyclic         | `stroke_cyclical_set()`             | `cyclical_set()`                       |
| Set handle type           | `stroke_editcurve_set_handle_type()`| `set_handle_type()`                    |
| Stroke Flip               | `stroke_flip()`                     | `stroke_switch_direction()`            |
| Join stroke               | `stroke_join()`                     | `join_selection()`                     |
| Normalize thickness       | `stroke_normalize(mode='THICKNESS')`| `set_uniform_thickness()`              |
| Normalize opacity         | `stroke_normalize(mode='OPACITY')`  | `set_uniform_opacity()`                |
| Separate strokes          | `stroke_separate()`                 | `separate()`                           |
| Colors flip               | `tint_flip()`                       | `paint.brush_colors_flip()`            |
| Unlock all                | `unlock_all()`                      | `layer_lock_all(lock=False)`           |
| Vertex Groups             | `vertex_group_*()`                  | `object.vertex_group_*()`              |

### Annotations

Annotations still use the legacy Grease Pencil ID type to store their data.
For compatibilty reasons, the RNA types for annotations become the legacy Grease Pencil RNA types:

- `bpy.types.GreasePencil`: Annotation data-block.
- `bpy.types.GreasePencilLayers`: Annotation layer collection.
- `bpy.types.GPencilLayer`: Annotation layer.
- `bpy.types.GPencilFrames`: Annotation frames collection.
- `bpy.types.GPencilFrame`: Annotation frame.
- `bpy.types.GPencilStroke`: Annotation stroke.
- `bpy.types.GPencilStrokePoint`: Annotation stroke point.

This is because annotations use the same data-structure as the legacy Grease Pencil.

The API for annotations has been reduced to a minimum.
Users and add-on developers are encouraged to use Grease Pencil instead of annotations for more
complicated tasks.

Here is a description of the types and their properties
(see the [Blender Python API
documentation](https://docs.blender.org/api/current/bpy.types.GreasePencil.html)
for a full description):

- `bpy.types.GreasePencil`: Annotation data-block.
  - `animation_data` (`bpy.types.AnimData`):  Animation data for this data-block.
  - `layers` (`GreasePencilLayers` of `GPencilLayer`): Collection of annotation layers.
- `bpy.types.GreasePencilLayers`: Annotation layer collection.
  - `active_index` (`int`): Index of active annotation layer.
  - `active_note` (`GPencilLayer`): Note/Layer to add annotation strokes to.
  - `new(:name, :set_active?)`: Function to add a new annotation layer.
  - `remove(:layer)`: Function to remove an annotation layer.
- `bpy.types.GPencilLayer`: Annotation layer.
  - `active_frame` (`GPencilFrame`): Frame currently being displayed for this layer
  - `annotation_hide` (`bool`): Set annotation Visibility.
  - `annotation_onion_after_color` (`mathutils.Color`):
    Base color for ghosts after the active frame.
  - `annotation_onion_after_range` (`int`):
    Maximum number of frames to show after current frame.
  - `annotation_onion_before_color` (`mathutils.Color`):
    Base color for ghosts before the active frame.
  - `annotation_onion_before_range` (`int`):
    Maximum number of frames to show before current frame.
  - `annotation_onion_use_custom_color` (`bool`):
    Use custom colors for onion skinning instead of the theme.
  - `annotation_opacity` (`float`): Annotation Layer Opacity.
  - `color` (`mathutils.Color`): Color for all strokes in this layer.
  - `frames` (collection of `GPencilFrame`): Sketches for this layer on different frames.
  - `info` (`str`): Layer name.
  - `is_ruler` (`bool`): This is a special ruler layer.
  - `lock` (`bool`): Protect layer from further editing and/or frame changes.
  - `lock_frame` (`bool`): Lock current frame displayed by layer.
  - `select` (`bool`): Layer is selected for editing in the Dope Sheet.
  - `show_in_front` (`bool`): Make the layer display in front of objects.
  - `thickness` (`int`): Thickness of annotation strokes.
  - `use_annotation_onion_skinning` (`bool`):
    Display annotation onion skins before and after the current frame
- `bpy.types.GPencilFrames`: Annotation frames collection.
  - `new(:frame_number, :active?)`: Function to add a new annotation frame.
  - `remove(:frame)`: Function to remove an annotation frame.
  - `copy(:source)`: Function to copy the source frame.
- `bpy.types.GPencilFrame`: Annotation frame.
  - `frame_number` (`int`): The frame on which this sketch appears.
  - `select` (`bool`): Frame is selected for editing in the Dope Sheet.
  - `strokes` (collection of `GPencilStroke`): Freehand curves defining the sketch on this frame.
- `bpy.types.GPencilStroke`: Annotation stroke.
  - `points` (collection of `GPencilStrokePoint`): Stroke data points.
- `bpy.types.GPencilStrokePoint`: Annotation stroke point.
  - `co` (`mathutils.Vector`): Coordinates of this point.
