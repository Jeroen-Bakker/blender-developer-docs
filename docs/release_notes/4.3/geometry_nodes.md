# Blender 4.3: Geometry Nodes

## For Each Element

A new **For Each Element** zone has been added
(blender/blender@6e5e01e63058238c7ea1e8e814959b2ff5dd287d). It allows evaluating the nodes in the
zone for every element (e.g. point or face) of a geometry. The output of the evaluation for each
element is then joined into either new geometries or new attributes.

![Stars Example of For Each Element zone](images/foreach_element_zone.png)

Using this zone is great for many use-cases that were impossible or very complex before. However,
performance of simple field evaluation is way superior compared to evaluating the same math nodes
inside of the zone. Also see the commit message for more performance considerations.

## Grease Pencil

Geometry Nodes supports working with Grease Pencil data now. From the perspective of Geometry
Nodes, a Grease Pencil geometry is made up of a flat list of layers where each layer has curves.
Layers, curves and curve control points can have custom attributes.

![Grease Pencil Geometry Data](images/grease_pencil_geometry_data.png)

Many nodes that already work with curves have been updated to implicitly work on Grease Pencil
data too. In these cases, each layer is processed independently.

![Grease Pencil Curves Nodes](images/grease_pencil_curves_nodes.png)

To make use of all the curves editing features, Grease Pencil can be converted to curves and back
using the corresponding conversion nodes. The conversion is *not* fully lossless yet but all
general attributes and materials are propagated. The conversion nodes can also be used to use
Grease Pencil as input for generating other geometry types and to generate Grease Pencil data from
scratch in Geometry Nodes.
Note that Grease Pencil and other geometries use incompatible types of materials,
but both kinds of materials use the material socket.

When converting Grease Pencil to curves, each layer is turned into a separate curves instance.
This allows processing each layer as curves separately. When disabling the `Layers as Instances`
input the output will be a single curves geometry that contains the strokes of all layers.
Similarly, instances are turned into layers when converting curves to Grease Pencil.

![Grease Pencil Curves Conversion](images/grease_pencil_curves_conversion.png)

While layer names have to be unique on original Grease Pencil data, that is not true in Geometry
Nodes. This is surprisingly important for performance, because making layer names unique can be
quite costly. However, when applying a Grease Pencil modifier, all layers with the same name are
merged into one. This can also be done in geometry nodes explicitly with the new **Merge Layers**
node (blender/blender@53b9594ee27841c3fea13c19821cf5c067c2f574). Layers can be merged either by
name or by a custom group id.

![Merge Layers Node](images/merge_layers_node.png)

## Packed Bakes

Bakes created with the Bake node or Simulation zone can now be packed into the `.blend` file
(blender/blender@3ccfa65245dff7358798cafd63f0675d86fc0556). Previously, the baked data was always
stored in separate files. Packing them into the `.blend` file improves shareability and simplifies
getting started with baking. For very large bakes, it's still recommended to store them separately
though, because then they can be loaded on demand.

Bakes are packed by default, but the bake target can be customized per geometry nodes modifier or
even per individual bake. Once baked, the data can be losslessly packed and unpacked. This is also
integrated with the more general `Pack/Unpack Resources` operators.

![Bake Target](images/bake_target.png)

It's also possible to see how large a bake is now.

![Bake Size](images/bake_size.png)

## Gizmos

It's now possible to add gizmos to node groups
(blender/blender@24dc9a21b1fee24c87442f1a15598d34c9301507).
This allows editing the inputs to a node tree directly in the 3D viewport
without having to go into the node editor or even modifier stack.

![Grid with Gizmo](images/grid_with_gizmo.png)

In this initial version, there are three kinds of gizmos that one can use:

* **Linear Gizmo**: Allows controlling a scalar value with a linear movement in the viewport. This
  works well for setting dimensions, counts, etc.
* **Dial Gizmo**: Allows controlling a scale value with dial movement. This is great for inputting
  angles.
* **Transform Gizmo**: Allows controlling a transform matrix value. This is actually a combination
  of multiple gizmos for translation, rotation and scale. It also takes the Global/Local transform
  orientation in the 3D viewport into account.

When using those nodes, it's important to understand that they are not input values by themselves.
Instead they are just attached to other already existing input values and control those.

One only has to use these nodes when authoring node groups with gizmos,
but not when using gizmos added by someone else.

![Gizmo Nodes](images/gizmo_nodes.png)

Built-in nodes like the Transform Geometry node do not have gizmos yet,
but that will be added in a later release.

## Warnings

A new **Warning** node has been added that allows adding custom warnings to node groups
(blender/blender@e8b81065bcd7f1999f685801f690a6434ad3b116).

![Warning Node](images/warning_node.png)

By default, all warning types are propagated to the parent node groups. However, there is a new
*Warning Propagation* node setting that allows disabling that certain warnings are propagated
further up (blender/blender@1ee6a8fad540f2468df175b96befface55188b45).
This can be used by assets which may where internal nodes may have warnings, which are
not relevant to the end user.

![Warning Propagation Setting](images/node_warning_propagation.png)

The display of warnings in the Geometry Nodes modifier has been reorganized. They are now in a
panel and ordered by severity and alphabetically.

![Warnings in Modifier](images/warnings_in_modifier.png)

Warnings generated by nodes in a geometry nodes modifier are also accessible with Python.

```python
for warning in modifier.node_warnings:
    print(warning.type, warning.message)
```

## Geometry Names

Geometries can be named with the new **Set Geometry Name** node
(blender/blender@dc8e9678e1502f931a242a9573fda5168b3a34de).
Some nodes also initialize names based on object and collection names.
Names are propagated through most nodes. The main purpose of
giving geometries names is to simplify debugging of more complex instance hierarchies. For that
purpose, the spreadsheet can now also display the data if nested instances.

![Set Geometry Name Node](images/set_geometry_name_node.png)

## Utility Nodes

There are a couple smaller new nodes which are useful for some setups:

* New **Hash Value** node (blender/blender@294994e4b93e39053b9fd0933ca30e3ba72db116).
* New **Integer Math** node (blender/blender@bf04513decbe443233ea61ddb61f23aad06e7ebe).
* New **Matrix Determinant** node (blender/blender@22d78c5a8eeb920f81c7b273949b7227271c4d6f).
* The *Value to String* node supports converting integers without floating point precision issues
  now (blender/blender@44fbd6931763a99983b27cc04f5d076c048935f2).

![New Utility Nodes](images/utility_geometry_nodes.png)

## Other

* Node timings are more accurate now
  (blender/blender@8af75d2fcc120387f68ab9710a0d7d5b0e731146).
* The Skip checkbox in the simulation zone is now hidden to avoid accidentally clicking it
  (blender/blender@af0d41745050a1d22cca330cea213cb9a3b85efc).
* Sculpt mask is now accessible in node tools (blender/blender@98f0926825d7).
