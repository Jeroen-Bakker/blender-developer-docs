# Blender 4.1: Geometry Nodes

## Baking

- The **Bake** can save and load data from inside node groups so that its inputs
  don't have to be recomputed (blender/blender@00eaddbd51eea80eb9cc72431dd137953afbb344)
  ([manual](https://docs.blender.org/manual/en/4.1/modeling/geometry_nodes/geometry/operations/bake.html#bake-node)).
- The bake node and simulation baking can save references to geometry materials
  (blender/blender@2d2b087fcf40bdae4f88c5fbdb83b87e623a79a2).
  - Baked data-block references can be managed in the "Data-Block References" panel
    of the node editor sidebar.
- Duplicate data in bakes is deduplicated more effectively, resulting in much smaller
  bake files in cases with identical geometry or attributes
  (blender/blender@586fadd6d2747a313f0ec9c7f4e45bde10d08efe).
- Volumes can be baked as part with simulation nodes or the bake node
  (blender/blender@444e148976761490b6c137c012f39bce5e9424c5).
- Bake caches are no longer lost after undo
  (blender/blender@7bb1ce124131bb0a41bfb805094b840a67f8a719).

## Nodes

- The **Musgrave Texture** node was [replaced by an extended Noise
  Texture node](rendering.md#musgrave-texture).
- The **Menu Switch** node allows creating custom "enum" menus to
  allow switching between options in the group interface
  (blender/blender@5ad49f41429177d9ec0572f97bb9575fec34f152).
- The **Split to Instances** node allows separating a geometry into
  multiple pieces based on a group ID
  (blender/blender@5bee6bcedc86b7f8fa2d451186eff75a721bd62d).
- The **Index Switch** node allows choosing between an arbitrary
  number of inputs with an index
  (blender/blender@8d5aa6eed4b50027002670e3e36a5d3457ade65c).
- The **Sort Elements** node allows reordering geometry elements
  based on some sort key.
  (blender/blender@37b2c12cfa5eb8def2428ec7a7cf36328382dc34).
- The _Fill Curve_ node now has a "Group ID" input to limit filling to
  specific curve groups
  (blender/blender@90de0368cdf2bcbd8b096f6069ccaef5455b0f0c).
- Node tools are now supported in object mode
  (blender/blender@3fcd9c94255f28c3a9d704b2dc1985682d07c419).
- The **Rotate Rotation** node replaces the _Rotate Euler_ node as a
  faster and clearer way to modifier rotations
  (blender/blender@f4867c0d700e2705b4a1b05a4b49dfab3a483612).
- Five nodes have been changed to use the rotation socket introduced in
  the last release
  (blender/blender@600a133521813939973fb9ac4be2634de884fb17,
  blender/blender@49087a84d02d6a6c1ebcda072d643960e3785378).
  - *Distribute Points on Faces*
  - *Instance on Points*
  - *Rotate Instances*
  - *Transform Geometry*
  - *Object Info*
  - *Instance Rotation*
- The **Active Camera** input node gives the scene's current camera
  object
  (blender/blender@75f160ee96b93b7438c4e5900e93d7332fa1d323).
- The _Set Curve Normal_ node now has the ability to set free ("custom")
  normals (blender/blender@f63a7c1ee9).
- The _Viewer_ node overlay can now be displayed as text values directly
  instead of colors (blender/blender@b85011aee0c1a20062818cc380b2b9e42f2092f8).
- The _Blackbody_ shader node is supported in geometry nodes
  (blender/blender@3f485c8bf363803c008a2ff366e83ae45d9b90bd).


## Modifier

- Node panels are now displayed in the modifier interface,
  making it much simpler to organize
  (blender/blender@ad7a5abb2da15563bfd61c455421b82b5c400f75).
- The panels in the geometry nodes modifier have been reorganized
  (blender/blender@9df4ffc0c2cc6e6295f83b03768e5059f4f217ca):
  - The `Output Attributes` panel is hidden when it is empty.
  - A new `Manage` panel contains a new `Bake` and `Named Attributes` panel.

## Performance

- The _Extrude Mesh_ can be over 6 times faster for large meshes with
  many vertex groups (blender/blender@6aaa74cda9).
- The _Shortest Edge Paths_ node can be at least 60% faster
  (blender/blender@c106066900754b70272e1111c546eed1746b4be9).
- The _Face Group Boundaries_ node can be over three times faster
  (blender/blender@5429e006c843cc68154f636003b1b75c5a8c593e).
- The _Edges to Face Groups_ node can be over seven times faster
  (blender/blender@274b2dbe5eb8a76f9702a06bf565c917f3ca3197,
  blender/blender@33442e099225fcad3e2f18731253f6be2f22aa57).

## Auto Smooth

- The mesh "Auto Smooth" option has been replaced by a modifier node
  group asset
  (blender/blender@89e3ba4e25c9ff921b2584c294cbc38c3d344c34).
  - This means geometry nodes now has the ability to set edge sharpness
    and create split normals without the need for an "original" mesh
    with the auto smooth option.
    - The behavior of sharp handles in the \*Curve to Mesh\* node is
      also controllable, and possible to create from scratch.
  - Blender automatically chooses whether to calculate vertex, face, or
    face corner normals given the presence of custom normals and the mix
    of sharp/smooth edges and faces.
  - Face corner "split" normals are calculated when there is a mix of
    sharp and smooth elements.
  - For more information on the impact to modeling, see the
    [Modeling](modeling.md) section.
  - For more information on the Python API changes, see the [Python
    API](python_api.md#breaking-changes)
    section.

![A node group asset to replace the behavior of the Auto Smooth option](../../images/Smooth_by_Angle_Modifier.png){style="width:400px;"}
