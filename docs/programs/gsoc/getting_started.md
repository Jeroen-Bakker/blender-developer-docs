# Google Summer of Code - Getting Started

**Welcome to GSoC!**

Google Summer of Code (GSoC) promotes open source software development
by sponsoring contributors to work on open source projects from the
comfort of their home. If you are a person interested in developing open
source software, this program is for you!

Blender is generally looking for contributors who are willing to learn,
have good coding skills and, most importantly, have a keen interest in
3D creation pipelines and open source software development in general.

## Conditions

Contributors who successfully complete GSoC will receive a
[compensation](https://developers.google.com/open-source/gsoc/help/student-stipends)
for their work.

To participate as a contributor, you must be at least 18 years old at
time of registration and an open source beginner. No group applications
are allowed.

<div class="grid cards" markdown>

- **Eligibility**

  [:octicons-arrow-right-24: Check your eligibility at the GSoC website](https://summerofcode.withgoogle.com/get-started)

</div>

GSoC is a program strictly for coding projects. Other initiatives, such
as for documentation or translation, are also very important, but are
not allowed for GSoC.

## First Steps

- **Google's [Student
  Guide](https://google.github.io/gsocguides/student) is the best place
  to start!**

  Read it carefully to get to know the initiative. The guide has a very good
  overview of the program and precious advice on how to get started,
  communicate, write your proposal and start the work. See also Google's
  [FAQ](https://developers.google.com/open-source/gsoc/faq).

- **Get familiar with the Blender project.**

  Investigate Blender's history, mission and get involved in the community.
  Browse the source code to get an idea of the project dimension and how it is
  organized.
  Be sure to know the basics of Blender's usability as an end-user!

- **Write your project proposal.**

  Head over to the [ideas page](ideas.md) to pick a topic that interests you.
  You can also take inspiration from [projects from previous years](index.md).

  !!! NOTE "Application Format"

      Your application must follow our [application template](application_template.md).

  We also recommend reading Google's [guide for writing a
  proposal](https://google.github.io/gsocguides/student/writing-a-proposal).

  Submit early if you want feedback! Don't expect any feedback on the last week
  before the deadline, as this is when most applications come in.

## Getting started with Blender

- New to Blender? [Download](https://www.blender.org/download/) it and
  do a number [of beginner tutorials for
  users](https://www.blender.org/support/tutorials/).
- Visit the [Get Involved
  section](https://www.blender.org/get-involved/developers/) which has
  information for new developers.
- Get the source and [build Blender](../../handbook/building_blender/index.md).
- Try to do one of the [good first
  issues](https://projects.blender.org/blender/blender/issues?labels=302),
  fix a [bug](https://projects.blender.org/blender/blender/issues), or
  provide another patch for Blender.
  Your proposal should link to your patches or previous contributions to
  Blender.
  Don't be intimidated! If this is your first time approaching a big
  code base, see our
  [New Developer Introduction](../../handbook/new_developers/index.md).

## FAQ

**Q: What will most help my odds of acceptance?**

: A: There are a number of things you can do to help your odds

  1. A quality proposal - a well thought out proposal that shows you understand
  what you want to do and have reasonable expectations about what can be
  accomplished in the time you will have available.
  1. Show evidence of past experience or achievements related to the proposal.
  1. Make a useful patch for Blender - this shows that you can successfully
  read, edit, and compile our code.
  1. Discuss your application with us and get some feedback. You can chat with
  us on [chat.blender.org](https://chat.blender.org/) and post a link to your proposal and of course we will
  give feedback in the GSoC web-interface too.

**Q: Is there some place that tells me the process of compiling Blender?**

:  A: See [this documentation](../../handbook/building_blender/index.md).

**Q: Where can I learn more about Blender's code base?**

: A: There is quite a bit of good documentation [here](../../features/index.md).

**Q: The Blender code base is huge! Where do I start?**

: A: Have a look at the [Files structure](../../features/code_layout.md). The
  editor directory is usually a good place - it is where most of the operators
  live.  Have a look at the header files and structs related to what you are
  interested in working on. The headers usually have the best overview of what a
  function does.
  Another way to start is with writing Python scripts. The API for our Python
  tools is similar in many ways to our C API. You can often find out where some
  C code lives by seeing the python tooltips when hovering over a button and
  seeing what the operator name is.
  Some useful techniques are to scan the console window for output of actions
  and to place a breakpoint in a function to get familiar with the code by
  stepping through and examining the callstack.
  See more tips and techniques in the
  [New Developer Introduction](../../handbook/new_developers/index.md).

**Q: I want to get involved before submitting my application. Can you guide me?**

: A: Getting involved early and showing presence is a good idea and increases
  your chances for acceptance. On the other hand, contributors have to show that
  they can take steps by themselves, without much "handholding". Mentors will
  try to *help* contributors make it successfully through the project; their job
  is *not* to give guidance on every step of the way.
  Of course, the [usual communication channels](../../handbook/new_developers/contact.md) are available
  to contributors for when they need help on specific topics. Together with the
  [developer introduction guides](../../handbook/new_developers/index.md), contributors
  should be able to get started.

  So take some initiative yourself. Show that you are capable of working on your
  own (to a good degree), that you can figure simple things out yourself and
  that you know when and where to ask questions.

## Thanks!

Thank you for considering Blender for your Google Summer of Code!
